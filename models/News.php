<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ff_news".
 *
 * @property integer $id
 * @property string $title
 * @property string $path
 * @property string $preview
 * @property string $text
 * @property string $tags
 * @property integer $position
 * @property integer $is_hot
 * @property integer $is_visible
 * @property integer $created
 * @property integer $updated
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 */
class News extends \yii\db\ActiveRecord
{

    public $_image;
    public $_deleteImage;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ff_news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'text'], 'required'],
            [['text'], 'string'],
            [['title', 'path'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Id'),
            'title' => Yii::t('app', 'Title'),
            'path' => Yii::t('app', 'Path'),
            'preview' => Yii::t('app', 'Preview'),
            'text' => Yii::t('app', 'Text'),
            'tags' => Yii::t('app', 'Tags'),
            'position' => Yii::t('app', 'Position'),
            '_image' => Yii::t('app', 'Image'),
            '_deleteImage' => Yii::t('app', 'Delete image'),
            'is_hot' => Yii::t('app', 'Is hot'),
            'is_visible' => Yii::t('app', 'Visible'),
            'views' => Yii::t('app', 'Views'),
            'created' => Yii::t('app', 'Created'),
            'updated' => Yii::t('app', 'Updated'),
            'meta_title' => Yii::t('app', 'МЕТА: Title'),
            'meta_description' => Yii::t('app', 'МЕТА: Description'),
            'meta_keywords' => Yii::t('app', 'МЕТА: Keywords'),
        ];
    }
}
