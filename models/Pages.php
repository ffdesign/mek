<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ff_pages".
 *
 * @property integer $id
 * @property string $title
 * @property string $path
 * @property string $text
 * @property integer $is_visible
 * @property integer $views
 * @property string $metaTitle
 * @property string $metaDescription
 * @property string $metaKeywords
 * @property integer $created
 * @property integer $updated
 */
class Pages extends \yii\db\ActiveRecord
{
    public $_image;
    public $_deleteImage;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ff_pages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['text', 'text2'], 'string'],
            [['title', 'path'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'text2' => 'Текст: правая колонка',
            'text' => 'Текст',
            'path' => 'Путь',
            'is_menu' => 'Показыать в меню',
            'is_visible' => 'Видимость',
            '_image' => 'Изображение (263x343)',
            '_deleteImage' => Yii::t('app', 'Delete image'),
        ];
    }
}
