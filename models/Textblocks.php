<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ff_textblocks".
 *
 * @property integer $id
 * @property string $title
 * @property string $name
 * @property string $image
 * @property string $text
 * @property integer $is_visible
 * @property integer $created
 * @property integer $updated
 */
class Textblocks extends \yii\db\ActiveRecord
{

    public $_image;
    public $_deleteImage;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ff_textblocks';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text'], 'string'],
            [['is_visible', 'updated'], 'integer'],
            [['title', 'name', 'image', 'location'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Id'),
            'title' => Yii::t('app', 'Title'),
            'name' => Yii::t('app', 'System name'),
            '_image' => Yii::t('app', 'Image'),
            '_deleteImage' => Yii::t('app', 'Delete image'),
            'text' => Yii::t('app', 'Text'),
            'is_visible' => Yii::t('app', 'Visible'),
            'updated' => Yii::t('app', 'Updated'),
        ];
    }
}
