<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ff_infrastructure".
 *
 * @property integer $id
 * @property string $title
 * @property string $image
 * @property integer $is_visible
 */
class Infrastructure extends \yii\db\ActiveRecord
{
    public $_image;
    public $_deleteImage;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ff_infrastructure';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_visible'], 'integer'],
            [['title', 'image'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок (отображается только в панели администратора)',
            '_image' => 'изображение',
            'is_visible' => 'Показывать',
        ];
    }
}
