<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ff_news_links".
 *
 * @property integer $id
 * @property string $url
 * @property integer $status
 */
class NewsLinks extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ff_news_links';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['url'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'url' => 'Заголовок',
            'status' => 'Показывать',
        ];
    }
}
