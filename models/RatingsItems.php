<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ff_ratings_items".
 *
 * @property integer $id
 * @property integer $id_rates
 * @property string $title
 * @property integer $ranking
 *
 * @property FfRatings $idRates
 */
class RatingsItems extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ff_ratings_items';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_rates', 'title'], 'required'],
            [['id_rates', 'ranking'], 'integer'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_rates' => 'Id Rates',
            'title' => 'Заголовок',
            'ranking' => 'Место в рейтинге',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRatings()
    {
        return $this->hasOne(Ratings::className(), ['id' => 'id_rates']);
    }
}
