<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\QuestsGallery;

/**
 * QuestsGallerySearch represents the model behind the search form about `app\models\QuestsGallery`.
 */
class QuestsGallerySearch extends QuestsGallery
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_quest', 'position', 'is_visible'], 'integer'],
            [['image'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = QuestsGallery::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'id_quest' => $this->id_quest,
            'position' => $this->position,
            'is_visible' => $this->is_visible,
        ]);

        $query->where(['id_quest' => $_GET['id']]);
        $query->orderBy('position desc');

        $query->andFilterWhere(['like', 'image', $this->image]);

        return $dataProvider;
    }
}