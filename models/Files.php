<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ff_files".
 *
 * @property integer $id
 * @property string $id_category
 * @property string $title
 * @property string $file
 * @property integer $size
 * @property string $extension
 * @property integer $created
 * @property integer $is_visible
 */
class Files extends \yii\db\ActiveRecord
{
    public $_file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ff_files';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_category', 'title', 'created'], 'required'],
            [['size', 'created', 'is_visible'], 'integer'],
            [['title', 'file'], 'string', 'max' => 255],
            [['extension'], 'string', 'max' => 25]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_category' => 'Id Category',
            'title' => 'Заголовок',
            '_file' => 'Файл',
            'size' => 'Размер файла',
            'extension' => 'Расширение файла',
            'created' => 'Дата',
            'is_visible' => 'Показывать',
        ];
    }
}
