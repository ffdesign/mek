<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "ff_information".
 *
 * @property integer $id
 * @property string $title
 * @property string $file
 * @property integer $created
 * @property integer $year
 * @property integer $is_visible
 */
class Information extends \yii\db\ActiveRecord
{
    public $_file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ff_information';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'created', 'year'], 'required'],
            [['year', 'is_visible'], 'integer'],
            [['file'], 'file'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            '_file' => 'Файл',
            'created' => 'Дата',
            'year' => 'Год',
            'is_visible' => 'Показывать',
        ];
    }

    public function getFiles($year) {
        return $this->find()->where(['is_visible' => 1, 'year' => $year])->all();
    }
}
