<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ff_ratings".
 *
 * @property integer $id
 * @property string $title
 * @property string $datetime
 *
 * @property FfRatingsItems[] $ffRatingsItems
 */
class Ratings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ff_ratings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'string', 'max' => 255],
            [['month', 'year'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'datetime' => 'Дата',
            'month' => 'Месяц',
            'year' => 'Год',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRatingsItems()
    {
        return $this->hasMany(RatingsItems::className(), ['id_rates' => 'id']);
    }
}
