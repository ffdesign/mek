<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'name' => 'Межрегиональная энергосбытовая компания',
    'language' => 'ru-RU',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            'enableCsrfValidation' => false,
            'enableCookieValidation' => true,
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'ZsxhQIkhJIAv9FG65dcq0_0oP8ijSp1h',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'cache' => [
            'class' => 'yii\caching\DbCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mail' => [
            'class' => 'yii\swiftmailer\Mailer',
            'transport' => [
            'class' => 'Swift_SmtpTransport',
            'host' => 'smtp.yandex.ru',  // e.g. smtp.mandrillapp.com or smtp.gmail.com
            'username' => 'info@f-f.bz',
            'password' => 'LpE6QRgTWG',
            'port' => '465', // Port 25 is a very common port too
            'encryption' => 'ssl', // It is often used, check your provider or mail server specs
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '<controller:(site)>/<action:index|about|contact|login|ratings>' => '<controller>/<action>',

                //Роутинг для модулей ОБЩИЙ
                '/admin/<module:\w+>/<action:manage|create|settings|template>' => '<module>/admin/<action>',
                '/admin/<module:\w+>/<action:update|delete|view>/<id:\d+>' => '<module>/admin/<action>',

                '/admin/<module:\w+>/items/<action:manage|create>/<id:\d+>' => '<module>/adminitems/<action>', 
                '/admin/<module:\w+>/items/<action:update|delete>/<id_ratings:\d+>/<id:\d+>' => '<module>/adminitems/<action>', //роутинг для шаблонов
                //Роутинг для модулей ИНДИВИДУАЛЬНЫЙ
                '<module:(page|news|quests)>/<slug>' => '<module>/default/view',
                '<module:(quests)>/<slug>' => '<module>/default/view',
                '<controller>/<action>/<id>' => '<controller>/<action>',
            ],
        ],
    ],
    'modules' => [
        'page' => [
            'class' => 'app\modules\page\Module',
        ],
        'news' => [
            'class' => 'app\modules\news\Module',
        ],
        'textblocks' => [
            'class' => 'app\modules\textblocks\module',
        ],
        'admin' => [
            'class' => 'app\modules\admin\Module',
        ],
        'users' => [
            'class' => 'app\modules\users\Module',
        ],
        'license' => [
            'class' => 'app\modules\license\Module',
        ],
        'infrastructure' => [
            'class' => 'app\modules\infrastructure\Module',
        ],
        'information' => [
            'class' => 'app\modules\information\Module',
        ],
        'files' => [
            'class' => 'app\modules\files\Module',
        ],
        'ratings' => [
            'class' => 'app\modules\ratings\Module',
        ],
        'friends' => [
            'class' => 'app\modules\friends\Module',
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
