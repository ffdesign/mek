<?php

namespace app\modules\news\controllers;

use Yii;
use app\models\News;
use app\modules\news\models\Settings;
use app\models\search\NewsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use app\helpers\YiiHelper;


/**
 * AdminController implements the CRUD actions for News model.
 */
class AdminController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    // 'delete' => ['post'],
                ],
            ],
        ];
    }

    public function init() 
    {
        if (\Yii::$app->user->isGuest || \Yii::$app->user->id != 1) {
            return $this->goHome();
        }
    }

    public function actionSettings() {
        // $post = Yii::$app->request->post();
        // if ($post) {
        //     DBFileHelper::set('news', 'perPage', (int)$post['Settings']['perPage']); 
        //     DBFileHelper::set('news', 'rssCount', (int)$post['Settings']['rssCount']); 
        // }

        // $model = new Settings;

        // $this->layout = '/admin';

        // return $this->render('settings', [
        //     'data' => DBFileHelper::getAll('news'),
        //     'model' => $model
        // ]);
    }

    /**
     * Lists all News models.
     * @return mixed
     */
    public function actionManage()
    {
        $searchModel = new NewsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $this->layout = '/admin';
        
        return $this->render('manage', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single News model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new News model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new News();

        $this->layout = '/admin';
        if (Yii::$app->request->post()) {

            $model->load(Yii::$app->request->post());
            $model->path = /*$id . date('ds', time()) .'-'. */YiiHelper::translite(Yii::$app->request->post()['News']['title']);
            $model->created = time();

            if($model->save()) {
                Yii::$app->session->setFlash('flashMessage', array('success', 'Новая запись добавлена.'));
                return $this->redirect(['/admin/' . $this->module->nameModule . '/manage']);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing News model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $this->layout = '/admin';
        if (Yii::$app->request->post()) {

            $model->load(Yii::$app->request->post());

            if($model->save()) {
                Yii::$app->session->setFlash('flashMessage', array('info', 'Запись обновлена.'));
                return $this->redirect(['/admin/' . $this->module->nameModule . '/manage']);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing News model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash('flashMessage', array('danger', 'Запись удалена.'));
        return $this->redirect(['/admin/' . $this->module->nameModule . '/manage']);
    }

    /**
     * Finds the News model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return News the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
