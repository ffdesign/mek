<?php

namespace app\modules\news\controllers;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use app\models\News;
use yii\data\Pagination;

class DefaultController extends Controller
{

	public function actionIndex() 
	{
		$model = new News;
        $query = $model->find()->orderBy('id ASC');

        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);
        $pages->setPageSize($this->module->perPage);
        
        $model = $query->offset($pages->offset)->limit($pages->limit)->all();

		return $this->render('index', [
            'model' => $model,
            'pages' => $pages,
        ]);
	}

    public function actionView($slug)
    {
        return $this->render('view', [
            'model' => $this->findModel($slug),
        ]);
    }
    /**
     * Finds the News model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return News the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {   
        if(is_numeric($id)) {
            $model = News::find()->where(['id'=>$id])->one();
        } else {
            $model = News::find()->where(['path'=>$id])->one();
        }
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
