<?php
/**
 * Module News
 * @author Roman Frank (Akella) 2akellaw@gmail.com
 */

namespace app\modules\news;

use Yii;

class module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\news\controllers';

    public $nameModule = 'news';
    public $uploadDir = 'images/news/';
    public $allowedExtensions = 'jpg,jpeg,png,gif';
    public $minSize = 0;
    public $maxSize = 5368709120;
    public $maxFiles = 1;
    public $rssCount = 10;
    public $perPage = 5;
    
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

    public function getName()
    {
        return Yii::t('app', ucfirst($this->nameModule));
    }
}
