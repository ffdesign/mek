<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('app', 'Settings');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'News'), 'url' => ['manage']];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>
<h1><?= Html::encode($this->title) ?></h1>

<?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'perPage')->textInput(['value' => @$data->perPage]) ?>

    <?= $form->field($model, 'rssCount')->textInput(['value' => @$data->rssCount]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Update'), ['class' => 'btn btn-success']) ?>
    </div>

<?php ActiveForm::end(); ?>