<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'News');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']) ?>
        <?//= Html::a(Yii::t('app', 'Settings'), ['settings'], ['class' => 'btn btn-info']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'format' => 'raw',
                'value' => function ($model) {                      
                        return  Html::a('<center><span class="glyphicon glyphicon-eye-open"></span></center>', ['/news/'.$model->path], ['target' => '_blank']);
                },
            ],
            [
                'attribute' => Yii::t('app', 'Title'),
                'format' => 'raw',
                'value' => function ($model) {                      
                        return Html::a($model->title, ['update', 'id' => $model->id]);
                },
            ],
           
            [
                'attribute' => Yii::t('app', 'Created'),
                'format' => 'raw',
                'value' => function ($model) {                      
                        return $model->created;
                },
            ],

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
            ],
            
        ],
    ]); ?>

</div>
