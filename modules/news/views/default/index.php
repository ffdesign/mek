<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\StringHelper;

$this->title = $this->context->module->getName() . ' | ' . $this->context->module->module->name;
?>



<div class="row">
    <div class="col-sm-9 col-sm-push-3 r">
        <div class="text">
            <h1>Новости</h1>
            <div class="news">
                <? foreach ($model as $news): ?>
                    <div class="one">
                        <div class="date"><?= $news->created ?></div>
                        <div class="title"><a href="/news/<?= $news->id ?>"><?= $news->title ?></a></div>
                        <p><?= str_replace('<br>\r\n', '', StringHelper::truncate($news->text, 180)); ?></p>
                    </div>
                <? endforeach; ?>
            </div>
            <? echo LinkPager::widget([
                'pagination' => $pages,
            ]); ?>
        </div>
    </div>
    
    <?php echo \Yii::$app->view->renderFile('@app/views/layouts/_left_menu_sidebar.php'); ?>
</div>

