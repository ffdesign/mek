<?php
use yii\helpers\Html;

$this->title = $model->title . ' | ' . $this->context->module->module->name;
?>
<div class="row">
    <div class="col-sm-9 col-sm-push-3 r">
        <div class="text">
            <div class="date"><?= $model->created ?></div><br>
            <h1><?= $model->title ?></h1>
            <div class="news">
                    <div class="one">
                        
                        <p><?= $model->text; ?></p>
                    </div>
            </div>
        </div>
    </div>
    
    <?php echo \Yii::$app->view->renderFile('@app/views/layouts/_left_menu_sidebar.php'); ?>
</div>

