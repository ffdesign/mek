<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'username',
            'email:email',
            'confirmed_at',
            // 'unconfirmed_email:email',
            // 'password_reset_token',
            'blocked_at',
            'registration_ip',
            'created_at:datetime',
            // 'updated_at',
            'last_auth:datetime',
            // 'last_useragent',
            // 'flags',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
