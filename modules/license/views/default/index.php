<?php
use yii\helpers\Html;
use app\helpers\YiiHelper;

$this->title = 'Лицензии и свидетельства | ' . $this->context->module->module->name;
?>

<div class="row">
	<div class="col-sm-9 col-sm-push-3 r">
		<div class="text">
			<h1>Лицензии и&nbsp;свидетельства</h1>
			<p class="big"><?= YiiHelper::getTextblock("license_desc") ?></p>
        
			<div class="license">
				<div class="row iblock">
					<? foreach ($model as $license): ?>
						<div class="col-md-3 col-sm-4 col-xs-6" <?= ($license->id == 2) ? "style='top: -8px;'" : ""?>>
	                        <a href="/images/license/<?= $license->id ?>/<?= $license->image ?>">
								<span class="pic"><img style="border: 1px solid #e4e4e4" src="/images/license/<?= $license->id ?>/159x230-<?= $license->image ?>" alt=""></span>
								<span class="title"><?= $license->title ?></span>
	                        </a>
						</div>
					<? endforeach; ?>
				</div>
			</div>
        </div>
	</div>
	<?php echo \Yii::$app->view->renderFile('@app/views/layouts/_left_menu_sidebar.php'); ?>
</div>