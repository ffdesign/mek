<?php

namespace app\modules\license\controllers;

use Yii;
use app\models\License;
use app\models\search\LicenseSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use app\helpers\YiiHelper;

/**
 * DefaultController implements the CRUD actions for License model.
 */
class AdminController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function init() 
    {
        if (\Yii::$app->user->isGuest || \Yii::$app->user->id != 1) {
            return $this->goHome();
        }
    }

    /**
     * Lists all License models.
     * @return mixed
     */
    public function actionManage()
    {
        $searchModel = new LicenseSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $this->layout = '/admin';
        return $this->render('manage', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new License model.
     * If creation is successful, the browser will be redirected to the 'view' license.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new License();
        if (Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());

            if($model->find()->limit(1)->one())
                $lastId = $model->find()->orderBy('id desc')->limit(1)->one()->id + 1;
            else
                $lastId = 1;
            
            //Загрузка изображения
            $files = $_FILES['License'];
            if($files['name']['_image'])
                YiiHelper::uploadImage($model, $files, '_image', $lastId, $this->module->uploadDir, array('159, 230, 300'));

            if($model->save()) {
                Yii::$app->session->setFlash('flashMessage', array('success', 'Новая запись добавлена.'));
                return $this->redirect(['/admin/' . $this->module->nameModule . '/manage']);
            }
        } else {
            $this->layout = '/admin';
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing License model.
     * If update is successful, the browser will be redirected to the 'view' license.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());

            // //Удаление изображения
            // if(isset(Yii::$app->request->post()['License']['_deleteImage'])) {
            //     $model->image = '';
            //     YiiHelper::removeImage($this->module->uploadDir.$model->id);
            // }
            
            //Загрузка изображения
            $files = $_FILES['License'];
            if($files['name']['_image'])
                YiiHelper::uploadImage($model, $files, '_image', $model->id, $this->module->uploadDir, array('159, 230, 300'));

            if($model->save()) {
                Yii::$app->session->setFlash('flashMessage', array('info', 'Запись обновлена.'));
                return $this->redirect(['/admin/' . $this->module->nameModule . '/manage']);
            }
        } else {
            $this->layout = '/admin';
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing License model.
     * If deletion is successful, the browser will be redirected to the 'index' license.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash('flashMessage', array('danger', 'Запись удалена.'));
        return $this->redirect(['/admin/' . $this->module->nameModule . '/manage']);
    }

    /**
     * Finds the License model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return License the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = License::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested license does not exist.');
        }
    }
}
