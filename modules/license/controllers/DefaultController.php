<?php

namespace app\modules\license\controllers;

use Yii;
use app\models\License;
use app\models\search\LicenseSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DefaultController implements the CRUD actions for License model.
 */
class DefaultController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index', [
            'model' => License::find()->where(['is_visible' => 1])->all(),
        ]);
    }
}
