<?php

namespace app\modules\information\controllers;

use Yii;
use app\models\Information;
use app\models\search\InformationSearch;
use yii\web\UploadedFile;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use app\helpers\YiiHelper;

/**
 * DefaultController implements the CRUD actions for Information model.
 */
class AdminController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    // 'delete' => ['post'],
                ],
            ],
        ];
    }

    public function init() 
    {
        if (\Yii::$app->user->isGuest || \Yii::$app->user->id != 1) {
            return $this->goHome();
        }
    }

    /**
     * Lists all Information models.
     * @return mixed
     */
    public function actionManage()
    {
        $searchModel = new InformationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $this->layout = '/admin';
        return $this->render('manage', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Information model.
     * If creation is successful, the browser will be redirected to the 'view' information.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Information();
        if (Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());

            if($model->find()->limit(1)->one())
                $lastId = $model->find()->orderBy('id desc')->limit(1)->one()->id + 1;
            else
                $lastId = 1;
            
            //Загрузка файлов
            $this->_uploadFile($model, $lastId);

            $model->created = strtotime(Yii::$app->request->post()['Information']['created']);
            $model->year = date('Y', $model->created);

            if($model->save()) {
                Yii::$app->session->setFlash('flashMessage', array('success', 'Новая запись добавлена.'));
                return $this->redirect(['/admin/' . $this->module->nameModule . '/manage']);
            }
        } else {
            $this->layout = '/admin';
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Information model.
     * If update is successful, the browser will be redirected to the 'view' information.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());
            
            //Загрузка файлов
             if(isset($_FILES['Information']['name']['_file']) && !empty($_FILES['Information']['name']['_file'])) {
                $this->_uploadFile($model, $id);
            }
            
            $model->created = strtotime(Yii::$app->request->post()['Information']['created']);
            $model->year = date('Y', $model->created);

            if($model->save()) {
                Yii::$app->session->setFlash('flashMessage', array('info', 'Запись обновлена.'));
                return $this->redirect(['/admin/' . $this->module->nameModule . '/manage']);
            }
        } else {
            $this->layout = '/admin';
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Загрузка файлов
     * @param  object $model  
     * @param  int $lastId 
     * @return mixed
     */
    protected function _uploadFile($model, $lastId) {
        $model->_file = UploadedFile::getInstance($model, '_file');

        if (isset($model->_file) && !empty($model->_file)) {
            if(!file_exists('files/information/' . $lastId))
                mkdir('files/information/' . $lastId);
            else {
                if(file_exists('files/information/' . $lastId))
                    YiiHelper::removeImage('files/information/' . $lastId); //удалит все файлы из папки
                mkdir('files/information/' . $lastId);
            }         

            $model->_file->saveAs('files/information/' . $lastId . '/'  . md5($model->_file->baseName) . '.' . $model->_file->extension);
            $model->file = md5($model->_file->baseName) . '.' . $model->_file->extension;
        }
    }

    /**
     * Deletes an existing Information model.
     * If deletion is successful, the browser will be redirected to the 'index' information.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash('flashMessage', array('danger', 'Запись удалена.'));
        return $this->redirect(['/admin/' . $this->module->nameModule . '/manage']);
    }

    /**
     * Finds the Information model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Information the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Information::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested information does not exist.');
        }
    }
}
