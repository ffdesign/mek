<?php

namespace app\modules\information\controllers;

use Yii;
use app\models\Information;
use app\models\search\InformationSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DefaultController implements the CRUD actions for Information model.
 */
class DefaultController extends Controller
{
    public function actionIndex()
    {
    	$model = new Information;
        return $this->render('index', [
            'model' => $model,
            'years' => $model->find()->select(['year'])->where(['is_visible' => 1])->groupBy(['year'])->orderBy('year desc')->all(),
        ]);
    }
}
