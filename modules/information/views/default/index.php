<?php
use yii\helpers\Html;
use app\helpers\YiiHelper;

$this->title = 'Раскрытие информации | ' . $this->context->module->module->name;
?>

<div class="row">
	<div class="col-sm-9 col-sm-push-3 r">
		<div class="text">
			<h1>Раскрытие информации</h1>
			<div class="disclosure">

			<? foreach ($years as $year): ?>
				<div class="one">
					<div class="year"><?= $year->year ?></div>
					<div class="row">
						<? foreach($model->getFiles($year->year) as $key => $file): ?>
							<? if($key % 2 == 0): ?>
								<div class="col-xs-6">
									<ul>
		                            	<li><a href="/files/information/<?= $file->id ?>/<?= $file->file ?>"><?= $file->title ?></a></li>
		                            </ul>
								</div>
							<? else: ?>
								<div class="col-xs-6">
									<ul>
		                            	<li><a href="/files/information/<?= $file->id ?>/<?= $file->file ?>"><?= $file->title ?></a></li>
		                            </ul>
								</div>
							<? endif; ?>
						<? endforeach; ?>
					</div>
                </div>
			<? endforeach; ?>
			</div>
        </div>
	</div>
	<?php echo \Yii::$app->view->renderFile('@app/views/layouts/_left_menu_sidebar.php'); ?>
</div>