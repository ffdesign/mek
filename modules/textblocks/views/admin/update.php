<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Textblocks */

$this->title = Yii::t('app', 'Update') . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Textblocks'), 'url' => ['manage']];
$this->params['breadcrumbs'][] = ['label' => $model->title];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="textblocks-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
