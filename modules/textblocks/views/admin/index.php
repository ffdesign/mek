<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\textblocksSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Textblocks');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="textblocks-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => Yii::t('app', 'Title'),
                'format' => 'raw',
                'value' => function ($model) {                      
                        return Html::a($model->title, ['update', 'id' => $model->id]);
                },
            ],
            [
                'attribute' => Yii::t('app', 'Visible'),
                'format' => 'raw',
                'value' => function ($model) {                      
                        return $model->is_visible ? 'Да' : 'Нет';
                },
            ],

            
        ],
    ]); ?>

</div>
