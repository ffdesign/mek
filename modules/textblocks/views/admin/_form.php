<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Textblocks */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="alert alert-info" role="alert">
    <p>Текстовый блок имеет только одно состояние. Это <b>изображение</b> либо <b>текст</b>.</p>
    <p>Пока изображение не загружено, возможно редактировать текст, после того как вы загрузили изображение, возможность редактирования текста убирается и на месте текстового блока выводится изображение.</p>
</div>

<div class="textblocks-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 255, 'disabled' => 'disabled']) ?>

    <?= $form->field($model, '_image')->fileInput() ?>

    <?php if($model->image): ?>
        <?php $file = 'images/textblocks/' . $model->id . '/thumb-' . $model->image; ?>
        <img src="/<?=$file?>">
        <?= $form->field($model, '_deleteImage')->checkBox() ?>
    <?php endif; ?>

    <?php if(!$model->image): ?>
        <?php if($model->editor): ?>
            <?= $form->field($model, 'text')->widget(\yii\imperavi\Widget::classname(), [
                    'model' => $model,
                    'attribute' => 'text',

                    'options' => [
                        'formatting' => ['p', 'h1', 'h2', 'h3'],
                        'formattingAdd' => [
                            [
                                'tag' => 'vacancy',
                                'title' => 'Вакансия',
                            ],
                        ],
                        'allowedAttr' => ['span'],
                        'cleanStyleOnEnter' => false,
                        'cleanOnPaste' => false,
                        'paragraphize' => false,
                        'pastePlainText' => true,
                        'replaceDivs' => false,
                        'minHeight' => '200',
                        'lang' => 'ru',
                    ],
            ]); ?>
        <? else: ?>
            <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>
        <? endif; ?>
    <?php endif; ?>

    <?= $form->field($model, 'is_visible')->checkBox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
