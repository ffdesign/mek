<?php

namespace app\modules\textblocks;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\textblocks\controllers';

    public $name = 'Textblocks';
    public $nameModule = 'textblocks';
    public $uploadDir = 'images/textblocks/';
    
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
