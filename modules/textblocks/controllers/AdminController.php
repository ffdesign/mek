<?php

namespace app\modules\textblocks\controllers;

use Yii;
use app\models\Textblocks;
use app\models\search\textblocksSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\helpers\YiiHelper;

/**
 * AdminController implements the CRUD actions for Textblocks model.
 */
class AdminController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function init() 
    {
        if (\Yii::$app->user->isGuest || \Yii::$app->user->id != 1) {
            return $this->goHome();
        }
    }

    /**
     * Lists all Textblocks models.
     * @return mixed
     */
    public function actionManage()
    {
        $searchModel = new textblocksSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $this->layout = '/admin';
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Updates an existing Textblocks model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $this->layout = '/admin';
        if (Yii::$app->request->post()) {

            $model->load(Yii::$app->request->post());

            //Удаление изображения
            if(isset(Yii::$app->request->post()['Textblocks']['_deleteImage'])) {
                $model->image = '';
                YiiHelper::removeImage($this->module->uploadDir.$model->id);
            }
            
            //Загрузка изображения
            $files = $_FILES['Textblocks'];
            if($files['name']['_image'])
                YiiHelper::uploadImage($model, $files, '_image', $model->id, $this->module->uploadDir, array('200,300,100', '400,600,100'));


            if($model->save()) {
                Yii::$app->session->setFlash('flashMessage', array('info', 'Запись обновлена.'));
                return $this->redirect(['/admin/' . $this->module->nameModule . '/manage']);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Finds the Textblocks model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Textblocks the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Textblocks::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
