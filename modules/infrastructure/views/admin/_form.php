<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Pages */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pages-form">
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"><?= Yii::t('app', 'Default') ?></a></li>
    </ul>

    <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="home">
            <br>
            <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>
            
            <?= $form->field($model, '_image')->fileInput() ?>

            <?php if($model->image): ?>
                <?php $file = 'images/infrastructure/' . $model->id . '/thumb-' . $model->image; ?>
                <img src="/<?=$file?>">
                <br><br>
            <?php endif; ?>

            <?= $form->field($model, 'is_visible')->checkbox() ?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
