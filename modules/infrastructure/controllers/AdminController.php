<?php

namespace app\modules\infrastructure\controllers;

use Yii;
use app\models\Infrastructure;
use app\models\search\InfrastructureSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use app\helpers\YiiHelper;

/**
 * DefaultController implements the CRUD actions for Infrastructure model.
 */
class AdminController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function init() 
    {
        if (\Yii::$app->user->isGuest || \Yii::$app->user->id != 1) {
            return $this->goHome();
        }
    }

    /**
     * Lists all Infrastructure models.
     * @return mixed
     */
    public function actionManage()
    {
        $searchModel = new InfrastructureSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $this->layout = '/admin';
        return $this->render('manage', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Infrastructure model.
     * If creation is successful, the browser will be redirected to the 'view' infrastructure.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Infrastructure();
        if (Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());

            if($model->find()->limit(1)->one())
                $lastId = $model->find()->orderBy('id desc')->limit(1)->one()->id + 1;
            else
                $lastId = 1;
            
            //Загрузка изображения
            $files = $_FILES['Infrastructure'];
            if($files['name']['_image'])
                YiiHelper::uploadImage($model, $files, '_image', $lastId, $this->module->uploadDir);

            if($model->save()) {
                Yii::$app->session->setFlash('flashMessage', array('success', 'Новая запись добавлена.'));
                return $this->redirect(['/admin/' . $this->module->nameModule . '/manage']);
            }
        } else {
            $this->layout = '/admin';
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Infrastructure model.
     * If update is successful, the browser will be redirected to the 'view' infrastructure.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());

            // //Удаление изображения
            // if(isset(Yii::$app->request->post()['Infrastructure']['_deleteImage'])) {
            //     $model->image = '';
            //     YiiHelper::removeImage($this->module->uploadDir.$model->id);
            // }
            
            //Загрузка изображения
            $files = $_FILES['Infrastructure'];
            if($files['name']['_image'])
                YiiHelper::uploadImage($model, $files, '_image', $model->id, $this->module->uploadDir);

            if($model->save()) {
                Yii::$app->session->setFlash('flashMessage', array('info', 'Запись обновлена.'));
                return $this->redirect(['/admin/' . $this->module->nameModule . '/manage']);
            }
        } else {
            $this->layout = '/admin';
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Infrastructure model.
     * If deletion is successful, the browser will be redirected to the 'index' infrastructure.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash('flashMessage', array('danger', 'Запись удалена.'));
        return $this->redirect(['/admin/' . $this->module->nameModule . '/manage']);
    }

    /**
     * Finds the Infrastructure model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Infrastructure the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Infrastructure::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested infrastructure does not exist.');
        }
    }
}
