<?php

namespace app\modules\admin\controllers;

use Yii;
use yii\web\Controller;

class DefaultController extends Controller
{

	public function init()
	{
		if (\Yii::$app->user->isGuest || \Yii::$app->user->id != 1) {
            $this->redirect('/user');
        }
	}

    public function actionIndex()
    {	
    	$this->layout = '/admin';
        return $this->render('index');
    }
}
