<?php

namespace app\modules\files;
use Yii;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\files\controllers';

    public $uploadDir = 'images/files/';
    public $allowedExtensions = 'jpg,jpeg,png,gif';
    public $minSize = 0;
    public $maxSize = 5368709120;
    public $maxFiles = 1;
    public $rssCount = 10;
    public $perPage = 10;
    public $name = 'Pages';
    public $nameModule = 'files';

    public function init()
    {
        parent::init();
    }

    public function getName()
    {
        return Yii::t('app', $this->name);
    }
}
