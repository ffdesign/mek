<?php

namespace app\modules\files\controllers;

use Yii;
use app\models\Files;
use app\models\search\FilesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DefaultController implements the CRUD actions for Files model.
 */
class DefaultController extends Controller
{
    public function actionIndex()
    {
    	$model = new Files;
        return $this->render('index', [
            'model' => $model,
            'years' => $model->find()->select(['year'])->where(['is_visible' => 1])->groupBy(['year'])->orderBy('year desc')->all(),
        ]);
    }
}
