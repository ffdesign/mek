<?php

namespace app\modules\page;
use Yii;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\page\controllers';

    public $uploadDir = 'images/pages/';
    public $allowedExtensions = 'jpg,jpeg,png,gif';
    public $minSize = 0;
    public $maxSize = 5368709120;
    public $maxFiles = 1;
    public $rssCount = 10;
    public $perPage = 10;
    public $name = 'Pages';
    public $nameModule = 'page';

    public function init()
    {
        parent::init();
    }

    public function getName()
    {
        return Yii::t('app', $this->name);
    }
}
