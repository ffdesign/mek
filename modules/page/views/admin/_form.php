<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Pages */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pages-form">
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"><?= Yii::t('app', 'Default') ?></a></li>
        <li role="presentation"><a href="#right-column" aria-controls="right-column" role="tab" data-toggle="tab"><?= Yii::t('app', 'Right column') ?></a></li>
    </ul>

    <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="home">
            <br>
            <? if($model->id != 5): ?>
                <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>
                
                <? if(!$model->isNewRecord): ?>
                    <?= $form->field($model, 'path')->textInput(['maxlength' => 255, 'disabled' => true]) ?>
                <? endif; ?>
            <? endif; ?>

            <?= $form->field($model, 'text')->widget(\yii\imperavi\Widget::classname(), [
                    'model' => $model,
                    'attribute' => 'text',
                    'options' => [
                        'minHeight' => '200',
                        'lang' => 'ru',
                        'imageUpload' => '/site/upload-image',
                        'fileUpload' => '/site/upload-file',
                    ],
            ]); ?>
            <? if(!in_array($model->id, array(1,2,3,4,5))): ?>
                <?= $form->field($model, 'is_menu')->checkbox() ?>

                <?= $form->field($model, 'is_visible')->checkbox() ?>
            <? endif; ?>
        </div>
        <div role="tabpanel" class="tab-pane" id="right-column">
            <br>
            <?= $form->field($model, '_image')->fileInput() ?>

            <?php if($model->image): ?>
                <?php $file = 'images/pages/' . $model->id . '/thumb-' . $model->image; ?>
                <img src="/<?=$file?>">
                <br><br>
            <?php endif; ?>

            <?= $form->field($model, 'text2')->widget(\yii\imperavi\Widget::classname(), [
                    'model' => $model,
                    'attribute' => 'text2',
                    'options' => [
                        'minHeight' => '200',
                        'lang' => 'ru',
                        'imageUpload' => '/site/upload-image',
                        'fileUpload' => '/site/upload-file',
                    ],
            ]); ?>


        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
