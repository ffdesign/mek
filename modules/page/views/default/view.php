<?php
use yii\helpers\Html;
use app\helpers\YiiHelper;

$this->title = $model->title . ' | ' . $this->context->module->module->name;
?>

<div class="row">
	<!-- col-sm-6 - если будет вторая колонка -->
	<div class="<?php echo !empty($model->text2) ? 'col-sm-6' : 'col-sm-9' ?> col-sm-push-3 ">
		<div class="text">
			<h1><?= Html::encode($model->title) ?></h1>
			<?= $model->text ?>
        </div>
	</div>

	<? if(!empty($model->text2)): ?>
		<div class="col-sm-3 col-sm-push-3 r">
			<div class="person">
				<div class="pic">
					<? if($model->image): ?>
			            <img src="/images/pages/<?= $model->id ?>/<?= $model->image ?>"/>
			        <? endif; ?>
				</div>
				<!-- <div class="name">Пахомов Андрей Викторович</div> -->
				<!-- <div class="who">генеральный директор</div> -->
				<?= $model->text2 ?>
			</div>
		</div>
	<? endif; ?>
	
	<?php echo \Yii::$app->view->renderFile('@app/views/layouts/_left_menu_sidebar.php'); ?>
</div>