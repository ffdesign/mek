<?php

namespace app\modules\page\controllers;

use Yii;
use app\models\Pages;
use app\models\search\PagesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DefaultController implements the CRUD actions for Pages model.
 */
class DefaultController extends Controller
{
    public function actionView($slug)
    {
        if($slug == 'glavnaya-stranica') {
            $this->redirect('/');
        }
        return $this->render('view', [
            'model' => $this->findModel($slug),
        ]);
    }
    /**
     * Finds the Pages model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {   
        if(is_numeric($id)) {
            $model = Pages::find()->where(['id'=>$id, 'is_visible' => 1])->one();
        } else {
            $model = Pages::find()->where(['path'=>$id, 'is_visible' => 1])->one();
        }
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
