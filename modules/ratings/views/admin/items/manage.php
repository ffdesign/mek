<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\PagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Ratings: items');
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['/admin/ratings/manage']];
$this->params['breadcrumbs'][] = $datetime;
?>
<div class="pages-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create'), ['create', 'id' => $_GET['id']], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // [
            //     'format' => 'raw',
            //     'value' => function ($model) {                      
            //             return  Html::a('<center><span class="glyphicon glyphicon-eye-open"></span></center>', ['/page/'.$model->path], ['target' => '_blank']);
            //     },
            // ],

            [
                'attribute' => Yii::t('app', 'Title'),
                'format' => 'raw',
                'value' => function ($model) {                      
                        return Html::a($model->title, ['update', 'id' => $model->id]);
                },
            ],
            // [
            //     'attribute' => Yii::t('app', 'Path'),
            //     'format' => 'raw',
            //     'value' => function ($model) {                      
            //             return Html::a($model->path, ["/page/$model->path"]);
            //     },
            // ],

            // [
            //     'attribute' => Yii::t('app', 'Visible'),
            //     'format' => 'raw',
            //     'value' => function ($model) {                      
            //             return $model->is_visible ? 'Да' : 'Нет';
            //     },
            // ],

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span> ', '/admin/ratings/items/update/' .$_GET['id'].  '/'.$model->id);
                    }, 
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span> ', '/admin/ratings/items/delete/' .$_GET['id'].  '/'.$model->id);
                    }, 
                ],
            ],
        ],
    ]); ?>

</div>
