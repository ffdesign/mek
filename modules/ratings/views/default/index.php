<?php
use yii\helpers\Html;
use app\helpers\YiiHelper;

$this->title = 'Рейтинги | ' . $this->context->module->module->name;
?>

<div class="row">
	<div class="col-sm-9 col-sm-push-3 r">
		<div class="text">
			<h1><?= YiiHelper::getTextblock("module_ratings_title") ?><a href="#" class="rate-btn"><?= mb_strtolower($data['months'][$data['month'][0]->month]) ?> <?= $data['year'][0]->year ?></a></h1>
			<div class="rate-content"></div>
			<p class="small"><?= YiiHelper::getTextblock("module_ratings_desc") ?></p>
        </div>
		<div class="rate-tip">
			<form action="" class="rate-form">
				<div class="one">
					<label for="rate_year">Год:</label>
					<select name="rate_year" id="rate_year">
						<? $i=0; foreach ($data['year'] as $item): $i++; ?>
							<option value="<?= $item->year ?>" <?= ($i==1) ? 'selected' : '' ?>><?= $item->year ?></option>
						<? endforeach; ?>
					</select>
				</div>
				<div class="one">
					<label for="rate_month">Месяц:</label>
					<select name="rate_month" id="rate_month">
						<? $i=0; foreach ($data['month'] as $item): $i++; ?>
							<option value="<?= $item->month ?>" <?= ($i==1) ? 'selected' : '' ?>><?= $data['months'][$item->month] ?></option>
						<? endforeach; ?>
					</select>
				</div>
				<div class="one submit"><a href="#" id="getRatings" class="btn">Показать</a></div>
			</form>
        </div>
	</div>
	<?php echo \Yii::$app->view->renderFile('@app/views/layouts/_left_menu_sidebar.php'); ?>
</div>

<script type="text/javascript">
	var year = $('#rate_year').val(),
		month = $('#rate_month').val();

	$.ajax({
		type: "POST",
		url: "/site/ratings",
		data: {'year' : year, 'month' : month},
		success: function(msg) {
			$('.rate-content').html(msg);
		}
	});
</script>