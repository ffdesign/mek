<?php

namespace app\modules\ratings\controllers;

use Yii;
use app\models\Ratings;
use app\models\search\RatingsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use app\helpers\YiiHelper;

/**
 * DefaultController implements the CRUD actions for Ratings model.
 */
class AdminController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                ],
            ],
        ];
    }

    public function init() 
    {
        if (\Yii::$app->user->isGuest || \Yii::$app->user->id != 1) {
            return $this->goHome();
        }
    }

    /**
     * Lists all Ratings models.
     * @return mixed
     */
    public function actionManage()
    {
        $searchModel = new RatingsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $this->layout = '/admin';
        return $this->render('manage', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Ratings model.
     * If creation is successful, the browser will be redirected to the 'view' ratings.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Ratings();
        if (Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());
            $model->datetime = strtotime(Yii::$app->request->post()['Ratings']['datetime']);
            $model->month = date('m', $model->datetime);
            $model->year = date('Y', $model->datetime);
            print_r($model);
            if($model->save()) {
                Yii::$app->session->setFlash('flashMessage', array('success', 'Новая запись добавлена.'));
                return $this->redirect(['/admin/' . $this->module->nameModule . '/manage']);
            }
        } else {
            $this->layout = '/admin';
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Ratings model.
     * If update is successful, the browser will be redirected to the 'view' ratings.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());
            $model->datetime = strtotime(Yii::$app->request->post()['Ratings']['datetime']);
            $model->month = date('m', $model->datetime);
            $model->year = date('Y', $model->datetime);
            if($model->save()) {
                Yii::$app->session->setFlash('flashMessage', array('info', 'Запись обновлена.'));
                return $this->redirect(['/admin/' . $this->module->nameModule . '/manage']);
            }
        } else {
            $this->layout = '/admin';
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Ratings model.
     * If deletion is successful, the browser will be redirected to the 'index' ratings.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash('flashMessage', array('danger', 'Запись удалена.'));
        return $this->redirect(['/admin/' . $this->module->nameModule . '/manage']);
    }

    /**
     * Finds the Ratings model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Ratings the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Ratings::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested ratings does not exist.');
        }
    }
}
