<?php

namespace app\modules\ratings\controllers;

use Yii;
use app\models\Ratings;
use app\models\search\RatingsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DefaultController implements the CRUD actions for Ratings model.
 */
class DefaultController extends Controller
{
    public function actionIndex()
    {
    	$model = new Ratings;
    	$data['year'] = $model->find()->select(['year'])->groupBy(['year'])->orderBy('year desc')->all();
    	$data['month'] = $model->find()->select(['month'])->groupBy(['month'])->orderBy('month asc')->all();
    	$data['months'] = array('1' => 'Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь');
        return $this->render('index', [
            'data' => $data,
        ]);
    }
}
