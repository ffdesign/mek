<?php

namespace app\modules\ratings\controllers;

use Yii;
use app\models\Ratings;
use app\models\RatingsItems;
use app\models\search\RatingsItemsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use app\helpers\YiiHelper;

/**
 * DefaultController implements the CRUD actions for RatingsItems model.
 */
class AdminitemsController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                ],
            ],
        ];
    }

    public function init() 
    {
        if (\Yii::$app->user->isGuest || \Yii::$app->user->id != 1) {
            return $this->goHome();
        }
    }

    /**
     * Lists all RatingsItems models.
     * @return mixed
     */
    public function actionManage($id)
    {
        $searchModel = new RatingsItemsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $datetime = Ratings::find()->where(['id' => $id])->select('datetime')->one();

        $this->layout = '/admin';
        return $this->render('/admin/items/manage', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'datetime' => date('m.Y', $datetime->datetime),
        ]);
    }

    /**
     * Creates a new RatingsItems model.
     * If creation is successful, the browser will be redirected to the 'view' ratings.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = new RatingsItems();
        if (Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());
            $model->id_rates = $id;
            if($model->save()) {
                Yii::$app->session->setFlash('flashMessage', array('success', 'Новая запись добавлена.'));
                return $this->redirect(['/admin/' . $this->module->nameModule . '/items/manage/' . $id]);
            }
        } else {
            $this->layout = '/admin';
            return $this->render('/admin/items/create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing RatingsItems model.
     * If update is successful, the browser will be redirected to the 'view' ratings.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id_ratings, $id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());
            // $model->id_rates = $id_ratings;
            if($model->save()) {
                Yii::$app->session->setFlash('flashMessage', array('info', 'Запись обновлена.'));
                return $this->redirect(['/admin/' . $this->module->nameModule . '/items/manage/' . $id_ratings]);
            }
        } else {
            $this->layout = '/admin';
            return $this->render('/admin/items/update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing RatingsItems model.
     * If deletion is successful, the browser will be redirected to the 'index' ratings.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id_ratings, $id)
    {
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash('flashMessage', array('danger', 'Запись удалена.'));
        return $this->redirect(['/admin/' . $this->module->nameModule . '/items/manage/' . $id_ratings]);
    }

    /**
     * Finds the RatingsItems model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RatingsItems the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RatingsItems::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested ratings does not exist.');
        }
    }
}
