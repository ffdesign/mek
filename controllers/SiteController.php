<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use app\models\Ratings;
use app\models\News;
use app\models\Pages;
use app\models\Infrastructure;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    // 'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        $indexPage = Pages::find()->select('text')->where(['id' => 5])->one();
        $news = News::find()->limit(3)->orderBy('id ASC')->all();
        $infrastructure = Infrastructure::find()->select('id, image')->where(['is_visible' => 1])->all();

        include('shd.php');
        $html = file_get_html('http://www.atsenergo.ru/img_grey/graph/' . date('dmY') . '_day_hubs_extra_indexes.xml');
        $html2 = file_get_html('http://www.atsenergo.ru/img_grey/graph_sib/' . date('dmY') . '_day_hubs_extra_indexes.xml');
        $data = new \SimpleXMLElement($html);
        $data2 = new \SimpleXMLElement($html2);

        $html_old = file_get_html('http://www.atsenergo.ru/img_grey/graph/' . date('dmY', strtotime('-1 day')) . '_day_hubs_extra_indexes.xml');
        $html2_old = file_get_html('http://www.atsenergo.ru/img_grey/graph_sib/' . date('dmY', strtotime('-1 day')) . '_day_hubs_extra_indexes.xml');
        $data_old = new \SimpleXMLElement($html_old);
        $data2_old = new \SimpleXMLElement($html2_old);
        // 0 - Центр
        // 1 - Юг
        // 2 - Урал
        // 3 - Восточная Сибирь
        // 4 - Западная Сибирь
        $graphics = array();
        for ($i=0; $i <= 2  ; $i++) { 
            $hour = 0;
            $count = 0;
            foreach ($data->hub[$i] as $key => $value) {
                $count += floatval($value['hubprice']);
                $graphics[$i][] = array('hour' => $hour++, 'value' => $value['hubprice']);
            }
        }

        for ($i=0; $i <= 1  ; $i++) { 
            $hour = 0;
            $count = 0;
            foreach ($data2->hub[$i] as $key => $value) {
                $count += floatval($value['hubprice']);
                $graphics[$i+3][] = array('hour' => $hour++, 'value' => $value['hubprice']);
            }
        }

        $avg = array();
        for ($i=0; $i <= 2  ; $i++) { 
            $count = 0;
            foreach ($data->hub[$i] as $key => $value) {
                $count += floatval($value['hubprice']);
            }
            $avg[$i][] = $count/24;
        }
        for ($i=0; $i <= 1  ; $i++) { 
            $count = 0;
            foreach ($data2->hub[$i] as $key => $value) {
                $count += floatval($value['hubprice']);
            }
            $avg[$i+3][] = $count/24;
        }

        $avg_old = array();
        for ($i=0; $i <= 2  ; $i++) { 
            $count = 0;
            foreach ($data_old->hub[$i] as $key => $value) {
                $count += floatval($value['hubprice']);
            }
            $avg_old[$i][] = $count/24;
        }
        for ($i=0; $i <= 1  ; $i++) { 
            $count = 0;
            foreach ($data2_old->hub[$i] as $key => $value) {
                $count += floatval($value['hubprice']);
            }
            $avg_old[$i+3][] = $count/24;
        }


        return $this->render('index', [
            'news' => $news,
            'indexPage' => $indexPage,
            'infrastructure' => $infrastructure,
            'graphics' => $graphics,
            'avg' => $avg,
            'avg_old' => $avg_old,
        ]);
    }

    public function actionTest()
    {
        
    }

    // public function actionLogin()
    // {
    //     if (!\Yii::$app->user->isGuest) {
    //         return $this->goHome();
    //     }

    //     $model = new LoginForm();
    //     if ($model->load(Yii::$app->request->post()) && $model->login()) {
    //         return $this->goBack();
    //     }
    //     return $this->render('login', [
    //         'model' => $model,
    //     ]);
    // }

    // public function actionLogout()
    // {
    //     Yii::$app->user->logout();

    //     return $this->goHome();
    // }

    public function actionContact()
    {
        return $this->render('contact');
    }

    public function actionServices()
    {
        return $this->render('services');
    }

    public function actionService1()
    {
        return $this->render('service1');
    }

    public function actionService2()
    {
        return $this->render('service2');
    }

    public function actionService3()
    {
        return $this->render('service3');
    }

    public function actionRatings() 
    {
        $this->enableCsrfValidation = false;
        if(Yii::$app->getRequest()->getIsAjax()) {
            $model = new Ratings;
            $post = $_POST;
            return $this->renderPartial('_ratings', [
                'model' => $model->find()->select('id')->where(['year' => $post['year'], 'month' => $post['month']])->one(),
            ]);
        } else {
            die('404');
        }
    }



    public function actionAbout()
    {
        return $this->render('about');
    }
}
