<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use app\models\User;
use app\models\Files;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class UserController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    // 'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        $user = '';
        if (!\Yii::$app->user->isGuest) {
            $user = Yii::$app->getUser()->getIdentity(false);
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect('/user');
        }
        return $this->render('index', [
            'model' => $model,
            'user' => $user,
        ]);
    }

    public function actionRegistration() 
    {
        if(Yii::$app->getRequest()->getIsAjax()) {
            $post = $_POST;
            $model = new User();
            $model->username = $post['login'];
            $model->name = $post['name'];
            $model->email = $post['email'];
            $model->password = $model->setPassword($post['password']);
            if($model->save()) {
                $message = "Спасибо за регистрацию, ваши данные:<br>";
                $message .= "Логин: " . $post['login'] . '<br>';
                $message .= "Пароль: " . $post['password'];

                Yii::$app->mail->compose()
                    ->setFrom('info@f-f.bz')
                    ->setTo($post['email'])
                    ->setSubject('Регистрация МЭК')
                    ->setHtmlBody($message)
                    ->send();
                return true;
            }
                
        }
    }

    public function actionForget()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new User;
        $type = '';
        $message = '';
        $type_password = '';
        if(isset($_POST['forget'])) {
            $query = $model->findByEmail($_POST['email']);
            $token = $model->generatePasswordResetToken();
            $query->reset_password = $token;
            if($query->save()) {
                $type = "forget";

                $message = "Здравствуйте! Для вашего email был инициирован процесс восстановления пароля. Если это были не вы, то игнорируйте это письмо.<br><br>";
                $message .= "Для продолжения просьба перейти по ссылке<br>";
                $message .= "http://test.rfrank.ru/user/forget?token=" . $token;

                Yii::$app->mail->compose()
                    ->setFrom('info@f-f.bz')
                    ->setTo($_POST['email'])
                    ->setSubject('Восстановление пароля – МЭК')
                    ->setHtmlBody($message)
                    ->send();
            }
        }

        if(isset($_GET['token']) && $model->findByPasswordResetToken($_GET['token'])) {
            if(isset($_POST['forget_password'])) {
                $update = $model->findByPasswordResetToken($_GET['token']);
                $update->password = $model->setPassword($_POST['password']);
                $update->removePasswordResetToken();
                if($update->save()) {
                    $message = 'Пароль изменен. Информация отправлена на ваш email.';
                    $mail = "Ваш пароль изменен, данные для входа:<br>";
                    $mail .= "Логин: " . $update->username . '<br>';
                    $mail .= "Пароль: " . $_POST['password'];

                    Yii::$app->mail->compose()
                        ->setFrom('info@f-f.bz')
                        ->setTo($update->email)
                        ->setSubject('Изменение пароля – МЭК')
                        ->setHtmlBody($mail)
                        ->send();
                }
            }
            
        } else {
            $type_password = "new_password";
        }
        

        return $this->render('forget', [
            'type' => $type,
            'type_password' => $type_password,
            'message' => $message,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->redirect('/user');
    }

    public function actionFiles($id)
    {
        if (\Yii::$app->user->isGuest) {
            return $this->redirect('/user');
        }
        $model = new Files;
        $category = array('1' => 'Типовые документы', 'Отчетная информация', 'Аналитика', 'Интернет-трейдинг');
        $model = $model->find()->where(['is_visible' => 1, 'id_category' => $id])->all();
        return $this->render('files', [
            'model' => $model,
            'category' => $category,
        ]);
    }
}
