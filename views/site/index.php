<?php

/* @var $this yii\web\View */

$this->title = $this->context->module->name;
?>
<div class="row">
    <div class="col-lg-9 col-md-8 col-sm-7 r">
        <div class="text">
            <h1>Компания сегодня</h1>
            <?= $indexPage->text ?>
        </div>
        <div class="news-list">
            <h3>Новости и события <span><a href="/news">Все новости</a></span></h3>
            <div class="row">
                <? foreach ($news as $item): ?>
                    <div class="col-md-4 col-sm-6 col-xs-6">
                        <div class="title"><a href="/news/<?= $item->id ?>"><?= $item->title ?></a></div>
                        <div class="date"><?= $item->created ?></div>
                    </div>
                <? endforeach; ?>
            </div>
        </div>
        <? if(isset($infrastructure)): ?>
            <div class="clients-list hidden-xs">
                <h3>Инфраструктура рынка</h3>
                <ul>
                    <? foreach ($infrastructure as $item): ?>
                        <li><a href="" rel="blank"><img src="/images/infrastructure/<?= $item->id ?>/<?= $item->image ?>" alt=""></a></li>
                    <? endforeach; ?>
                </ul>
            </div>
        <? endif; ?>
    </div>
    <div class="col-lg-3 col-md-4 col-sm-5 l">
        <div class="bls">
            <div class="bl">
                <h3>Индексы хабов <span><?= date("d.m.Y") ?></span></h3>
                <table>
                    <tbody><tr>
                        <td>Центр (ЕС)</td>
                        <td><div class="bl-num"><?= number_format($avg[0][0], 2, '.', ''); ?></div></td>
                        <td class="<?= (($avg[0][0]-$avg_old[0][0]) < 0) ? 'bl-down' : 'bl-up'?>"><span><?= abs(number_format(100-$avg[0][0]*100/$avg_old[0][0], 2, '.', '')) ?>%</span></td>
                    </tr>
                    <tr>
                        <td>Урал (EU)</td>
                        <td><div class="bl-num"><?= number_format($avg[2][0], 2, '.', ''); ?></div></td>
                        <td class="<?= (($avg[2][0]-$avg_old[2][0]) < 0) ? 'bl-down' : 'bl-up'?>"><span><?= abs(number_format(100-$avg[2][0]*100/$avg_old[2][0], 2, '.', '')) ?>%</span></td>
                    </tr>
                    <tr>
                        <td>Юг (ES)</td>
                        <td><div class="bl-num"><?= number_format($avg[1][0], 2, '.', ''); ?></div></td>
                        <td class="<?= (($avg[1][0]-$avg_old[1][0]) < 0) ? 'bl-down' : 'bl-up'?>"><span><?= abs(number_format(100-$avg[1][0]*100/$avg_old[1][0], 2, '.', '')) ?>%</span></td>
                    </tr>
                    <tr>
                        <td>В.Сибирь (SE)</td>
                        <td><div class="bl-num"><?= number_format($avg[3][0], 2, '.', ''); ?></div></td>
                        <td class="<?= (($avg[3][0]-$avg_old[3][0]) < 0) ? 'bl-down' : 'bl-up'?>"><span><?= abs(number_format(100-$avg[3][0]*100/$avg_old[3][0], 2, '.', '')) ?>%</span></td>
                    </tr>
                    <tr>
                        <td>З.Сибирь</td>
                        <td><div class="bl-num"><?= number_format($avg[4][0], 2, '.', ''); ?></div></td>
                        <td class="<?= (($avg[4][0]-$avg_old[4][0]) < 0) ? 'bl-down' : 'bl-up'?>"><span><?= abs(number_format(100-$avg[4][0]*100/$avg_old[4][0], 2, '.', '')) ?>%</span></td>
                    </tr>
                </tbody></table>
            </div>

            <script src="js/jquery.canvasjs.min.js"></script>
            <script>
                $(function() {

                    $("#graph_1").CanvasJSChart({
                        animationEnabled: false,
                        interactivityEnabled: false,
                        toolTip:{
                            enabled: true
                        },
                        axisY:{
                            lineThickness: 0,
                            gridThickness: 1,
                            tickThickness: 0,
                            gridColor: '#cccccc',
                            margin: 0,
                            labelFontSize: 12,
                            interval: 500

                        },
                        axisX:{
                            lineThickness: 1,
                            tickThickness: 0,
                            tickLength: 4,
                            lineColor: '#666666',
                            margin: 0,
                            labelFontSize: 12,
                            interval: 4

                        },
                        data: [
                            {
                                color: "#24a6d8",
                                type: "spline",
                                markerType: "none",
                                dataPoints: [
                                <? foreach($graphics[0] as $item): ?>
                                    { x: <?= $item['hour'] ?>, y: <?= $item['value'] ?> },
                                <? endforeach; ?>
                                ]
                            },
                            {
                                color: "#054ba1",
                                type: "spline",
                                markerType: "none",
                                dataPoints: [
                                    <? foreach($graphics[1] as $item): ?>
                                        { x: <?= $item['hour'] ?>, y: <?= $item['value'] ?> },
                                    <? endforeach; ?>
                                ]
                            },
                            {
                                color: "#000000",
                                type: "spline",
                                markerType: "none",
                                dataPoints: [
                                    <? foreach($graphics[2] as $item): ?>
                                        { x: <?= $item['hour'] ?>, y: <?= $item['value'] ?> },
                                    <? endforeach; ?>
                                ]
                            }
                        ]
                    });

                });
            </script>
            <div class="bl">
                <h5>Первая ценовая зона</h5>
                <div class="graph"><div id="graph_1" style="height:125px;width:100%;"><div class="canvasjs-chart-container" style="position: relative; text-align: left; cursor: auto;"><canvas class="canvasjs-chart-canvas" width="233" height="200" style="position: absolute;"></canvas><canvas class="canvasjs-chart-canvas" width="233" height="200" style="position: absolute;"></canvas><div class="canvasjs-chart-toolbar" style="position: absolute; right: 1px; top: 1px;"></div><a class="canvasjs-chart-credit" style="outline:none;margin:0px;position:absolute;right:3px;top:186px;color:dimgrey;text-decoration:none;font-size:10px;font-family:Lucida Grande, Lucida Sans Unicode, Arial, sans-serif" tabindex="-1" target="_blank" href="http://canvasjs.com/">CanvasJS.com</a></div></div></div>
                <div class="legend">
                    <div class="row">
                        <div class="col-xs-4" style="color:#24a6d8;"><div><?= number_format($avg[0][0], 2, '.', ''); ?> <span>центр</span></div></div>
                        <div class="col-xs-4" style="color:#054ba1;"><div><?= number_format($avg[1][0], 2, '.', ''); ?> <span>юг</span></div></div>
                        <div class="col-xs-4" style="color:#312d2e;"><div><?= number_format($avg[2][0], 2, '.', ''); ?> <span>урал</span></div></div>
                    </div>
                </div>
            </div>
            <script>
                $(function() {
                    $("#graph_2").CanvasJSChart({
                        animationEnabled: false,
                        interactivityEnabled: false,
                        toolTip:{
                            enabled: false
                        },
                        axisY:{
                            lineThickness: 0,
                            gridThickness: 1,
                            tickThickness: 0,
                            gridColor: '#cccccc',
                            margin: 0,
                            labelFontSize: 12,
                            interval: 500

                        },
                        axisX:{
                            lineThickness: 1,
                            tickThickness: 0,
                            tickLength: 4,
                            lineColor: '#666666',
                            margin: 0,
                            labelFontSize: 12,
                            interval: 4

                        },
                        data: [
                            {
                                color: "#24a6d8",
                                type: "spline",
                                markerType: "none",
                                dataPoints: [
                                    <? foreach($graphics[4] as $item): ?>
                                        { x: <?= $item['hour'] ?>, y: <?= $item['value'] ?> },
                                    <? endforeach; ?>
                                ]
                            },
                            {
                                color: "#054ba1",
                                type: "spline",
                                markerType: "none",
                                dataPoints: [
                                    <? foreach($graphics[3] as $item): ?>
                                        { x: <?= $item['hour'] ?>, y: <?= $item['value'] ?> },
                                    <? endforeach; ?>
                                ]
                            }
                        ]
                    });

                });
            </script>
            <div class="bl">
                <h5>Вторая ценовая зона</h5>
                <div class="graph"><div id="graph_2" style="height:125px;width:100%;"><div class="canvasjs-chart-container" style="position: relative; text-align: left; cursor: auto;"><canvas class="canvasjs-chart-canvas" width="233" height="200" style="position: absolute;"></canvas><canvas class="canvasjs-chart-canvas" width="233" height="200" style="position: absolute;"></canvas><div class="canvasjs-chart-toolbar" style="position: absolute; right: 1px; top: 1px;"></div><a class="canvasjs-chart-credit" style="outline:none;margin:0px;position:absolute;right:3px;top:186px;color:dimgrey;text-decoration:none;font-size:10px;font-family:Lucida Grande, Lucida Sans Unicode, Arial, sans-serif" tabindex="-1" target="_blank" href="http://canvasjs.com/">CanvasJS.com</a></div></div></div>
                <div class="legend">
                    <div class="row">
                        <div class="col-xs-4" style="color:#24a6d8;"><div><?= number_format($avg[4][0], 2, '.', ''); ?> <span>З.Сибирь</span></div></div>
                        <div class="col-xs-4" style="color:#054ba1;"><div><?= number_format($avg[3][0], 2, '.', ''); ?> <span>В.Сибирь</span></div></div>
                    </div>
                </div>
            </div>
        </div>
        <? if(isset($infrastructure)): ?>
            <div class="clients-list visible-xs">
                <h3>Инфраструктура рынка</h3>
                <ul>
                    <? foreach ($infrastructure as $item): ?>
                        <li><a href="" rel="blank"><img src="/images/infrastructure/<?= $item->id ?>/<?= $item->image ?>" alt=""></a></li>
                    <? endforeach; ?>
                </ul>
            </div><!-- javascript clone here from .clients-list.hidden-xs -->
        <? endif; ?>
    </div>
</div>