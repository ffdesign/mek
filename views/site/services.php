<?php
use yii\helpers\Html;
use app\helpers\YiiHelper;
$this->title = 'Услуги | ' . $this->context->module->name;
?>
<div class="row">
	<div class="col-sm-9 col-sm-push-3 r">
		<div class="text">
			<h1>Услуги</h1>
			<p class="big">&laquo;МЭК&raquo; предлагает вам широкий выбор доступных и&nbsp;качественных услуг для эффективного ведения вашего бизнеса.</p>

			<link rel="stylesheet" href="/css/royalslider.css" media="all">
			<script src="/js/jquery.royalslider.min.js"></script>
			<script>
				$(document).ready(function(){
					var slider = $(".royalSlider").royalSlider({
                        imageScalePadding: 0,
                        controlNavigation: false,
                        arrowsNav: false,
                        slidesSpacing: 0,
                        loop: true,
                        imageScaleMode: 'fill',
                        sliderDrag: false,
                        sliderTouch: false,
                        navigateByClick: false,
                        fadeinLoadedSlide: false,
                        transitionSpeed: 400
					}).data('royalSlider');

					$('.services .nav a').click(function(){
						if ($(this).hasClass('next')) {
                        	slider.next();
						} else {
                            slider.prev();
						}
						$('.services .txt .one:visible').css('position','absolute').fadeOut(400);
						$('.services .txt .one:eq('+slider.currSlideId+')').css('position','relative').fadeIn(400);
						return false
					});
				});
			</script>

			<div class="services">
				<div class="row">
					<div class="pic col-md-6">
                        <div class="royalSlider rsServices">
                            <div class="rsContent">
                                <a class="rsImg" href="/i/service-1.jpg"></a>
                                <a class="rsLink" href="/site/service1"></a>
						    </div>
                            <div class="rsContent">
                                <a class="rsImg" href="/i/service-2.jpg"></a>
                                <a class="rsLink" href="/site/service2"></a>
						    </div>
                            <div class="rsContent">
                                <a class="rsImg" href="/i/service-3.jpg"></a>
                                <a class="rsLink" href="/site/service3"></a>
						    </div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="txt">
							<div class="one">
								<h2><a href="/site/service1">Поставка<br> электроэнергии</a></h2>
								<p><?= YiiHelper::getTextblock("services_description_1") ?></p>
								<p class="moar"><a href="/site/service1">Подробнее &rarr;</a></p>
							</div>
							<div class="one">
								<h2><a href="/site/service2">Консалтинг<br> и&nbsp;ИТ услуги</a></h2>
								<p><?= YiiHelper::getTextblock("services_description_2") ?> </p>
								<p class="moar"><a href="/site/service2">Подробнее &rarr;</a></p>
							</div>
							<div class="one">
								<h2><a href="/site/service3">Учет<br> электроэнергии</a></h2>
								<p><?= YiiHelper::getTextblock("services_description_3") ?> </p>
								<p class="moar"><a href="/site/service3">Подробнее &rarr;</a></p>
							</div>
						</div>
						<div class="nav"><a href="#" class="prev"><i class="icons icon-prev"></i></a><a href="#" class="next"><i class="icons icon-next"></i></a></div>
					</div>
                </div>
			</div>
        </div>
	</div>
	<div class="col-sm-3 col-sm-pull-9 l">
		<div class="back"><a href="/">&larr;<span>Назад</span></a></div>
		<div class="sidebar">
			<ul>
				<li><a href="/site/service1">Поставка электроэнергии</a></li>
				<li><a href="/site/service2">Консалтинг и&nbsp;ИТ услуги</a></li>
				<li><a href="/site/service3">Учет электроэнергии</a></li>
			</ul>
		</div>
	</div>
</div>