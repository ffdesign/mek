<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\helpers\YiiHelper;

$this->title = 'Контакты | ' . $this->context->module->name;
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="contact">
    <div class="legend">
        <h1>Контакты</h1>
        <p><?= YiiHelper::getTextblock("contacts_address") ?></p>
        <p><?= YiiHelper::getTextblock("contacts_phone") ?></p>
        <p><?= YiiHelper::getTextblock("contacts_fax") ?></p>
        <p><?= YiiHelper::getTextblock("contacts_email") ?></p>
    </div>

    <script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    <script>
        var myMap;
        ymaps.ready(init);
        function init() {
            myMap = new ymaps.Map('map', {
                center: [55.820167,37.657502],
                zoom: 14,
                controls: []
            });

            myMap.behaviors.disable('scrollZoom');
            myMap.controls.add(new ymaps.control.ZoomControl({options: { position: { right: 10, top: 10 }}}));

            $(window).resize(function(){
                if ($('.ismobile').is(':visible')) {
                    delta = 0;
                } else {
                    delta = 235;
                }
            }).resize();

            var position = myMap.getGlobalPixelCenter();
            myMap.setGlobalPixelCenter([ position[0] - delta, position[1] ]);

            placemark = new ymaps.Placemark([55.820167,37.657502], { }, {
                iconLayout: 'default#image',
                iconImageHref: '/i/marker.png',
                iconImageSize: [77, 88],
                iconImageOffset: [-34, -88]
            });
            myMap.geoObjects.add(placemark);
        }
    </script>
    <div class="ismobile"></div>
    <div id="map"></div>
</div>