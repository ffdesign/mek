<?php
use yii\helpers\Html;
use app\helpers\YiiHelper;

$this->title = 'Учет электроэнергии | ' . $this->context->module->name;
?>
<div class="row">
	<div class="col-xs-12 r">
		<div class="text">
			<div class="service-3">
				<div class="row">
					<div class="col-sm-3 hidden-xs">
						<div class="service-3-bg"><img src="/i/service-3-bg.png" alt=""></div>
					</div>
					<div class="col-sm-9 col-xs-12">
						<div class="toggle">
							<?= YiiHelper::getTextblock("services_service3_block_1") ?>
                        </div>
                        <div class="row">
							<div class="col-md-6 col-sm-12 col-xs-6">
								<?= YiiHelper::getTextblock("services_service3_block_2") ?>
							</div>
							<div class="col-md-6 col-sm-12 col-xs-6">
								<?= YiiHelper::getTextblock("services_service3_block_3") ?>
							</div>
						</div>
					</div>
				</div>
            </div>
		</div>
    </div>
</div>