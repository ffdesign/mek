<? if(!empty($model->ratingsItems)): ?>
	<table>
		<thead>
			<tr>
				<th>Место в рейтинге</th>
				<th>Название компании</th>
			</tr>
        </thead>
		<tbody>
			<? $i = 0; foreach($model->ratingsItems as $item): $i++; ?>
				<tr>
					<td><span class="tbl-num"><?= $i ?></span></td>
					<td><?= $item->title ?></td>
				</tr>
			<? endforeach; ?>
		</tbody>
	</table>
<? else: ?>
	Информации за выбранный период времени нет. 
<? endif; ?>
