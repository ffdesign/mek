<?php
use yii\helpers\Html;
use app\helpers\YiiHelper;

$this->title = 'Консалтинг и ИТ услуги | ' . $this->context->module->name;
?>
<div class="row">
	<div class="col-xs-12 r">
		<div class="text">
			<div class="service-2">
				<div class="row">
					<div class="col-md-4 col-sm-6 col-xs-6">
						<div class="icon"><img src="/i/service-2-1.png" alt=""></div>
						<?= YiiHelper::getTextblock("services_service2_block_1") ?>
					</div>
					<div class="col-md-4 col-sm-6 col-xs-6">
						<div class="icon"><img src="/i/service-2-2.png" alt=""></div>
						<?= YiiHelper::getTextblock("services_service2_block_2") ?>
					</div>
					<div class="col-md-4 col-sm-6 col-xs-6">
						<div class="icon"><img src="/i/service-2-3.png" alt=""></div>
						<?= YiiHelper::getTextblock("services_service2_block_3") ?>
					</div>
					<div class="col-md-4 col-sm-6 col-xs-6">
						<div class="icon"><img src="/i/service-2-4.png" alt=""></div>
						<?= YiiHelper::getTextblock("services_service2_block_4") ?>
					</div>
					<div class="col-md-4 col-sm-6 col-xs-6">
						<div class="icon"><img src="/i/service-2-5.png" alt=""></div>
						<?= YiiHelper::getTextblock("services_service2_block_5") ?>
					</div>
					<div class="col-md-4 col-sm-6 col-xs-6">
						<div class="icon"><img src="/i/service-2-6.png" alt=""></div>
						<?= YiiHelper::getTextblock("services_service2_block_6") ?>
					</div>
				</div>
			</div>
        </div>
    </div>
</div>