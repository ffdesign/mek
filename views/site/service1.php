<?php
use yii\helpers\Html;
use app\helpers\YiiHelper;

$this->title = 'Поставка электроэнергии | ' . $this->context->module->name;
?>
<div class="content">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 r">
				<div class="text">
     				<div class="service-1">
						<div class="one one-1">
							<img src="/i/service-1-1-bg.png" alt="">
							<div class="number">1</div>
							<?= YiiHelper::getTextblock("services_service1_block_1") ?>
						</div>
                        <div class="one one-2">
							<img src="/i/service-1-2-bg.png" alt="">
							<div class="number">2</div>
							<?= YiiHelper::getTextblock("services_service1_block_2") ?>
						</div>
                        <div class="one one-3">
							<img src="/i/service-1-3-bg.png" alt="">
							<div class="number">3</div>
							<?= YiiHelper::getTextblock("services_service1_block_3") ?>
						</div>
					</div>
                </div>
			</div>
		</div>
	</div>
</div>