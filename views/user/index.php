<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Клиентам | ' . $this->context->module->name;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
	<div class="col-sm-9 col-sm-push-3 r">
		<div class="text">
			<h1>Клиентам</h1>
			<? if(empty($user)): ?>
				<p class="big">Для получения доступа к&nbsp;закрытым разделам сайта вам&nbsp;необходимо&nbsp;авторизоваться</p>
				<div class="clients">
					<div class="row">
						<div class="col-xs-6">
							<div class="login">
								<?php $form = ActiveForm::begin([
								        'id' => 'login-form',
								        'options' => ['class' => 'form-horizontal'],
								        'fieldConfig' => [
								            'labelOptions' => ['class' => 'col-lg-1 control-label'],
								        ],
							    	]); 
							    ?>
							        <div class="one"><?= $form->field($model, 'username')->label('') ?></div>
							        <div class="one"><?= $form->field($model, 'password')->label('')->passwordInput() ?></div>
									<div class="one submit"><?= Html::submitButton('Войти', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?> <span class="forget"><a href="/user/forget">Напомнить пароль</a></span></div>
							    <?php ActiveForm::end(); ?>
					
							</div>
						</div>
						<div class="col-xs-6">
							<div class="register">
								<h3>Завести аккаунт</h3>
								<p>Если вы&nbsp;хотите получить доступ к&nbsp;закрытым разделам сайта, вам необходимо завести аккаунт, корректно заполнив все поля формы.</p>
								<p><a href="" class="btn" data-popup="popup_register">Завести аккаунт</a></p>
							</div>
						</div>
					</div>
				</div>
				<div class="popup text mfp-hide" id="popup_register">
					<h2>Завести аккаунт</h2>
					<form id='registration'>
						<div class="one"><input type="text" placeholder="ФИО" name="name" id="name" class=" name required"></div>
						<div class="one"><input type="text" placeholder="Логин" name="login" id="login" class=" login required"></div>
						<div class="one"><input type="password" placeholder="Пароль" name="password" id="password" class=" password required"></div>
						<div class="one"><input type="text" placeholder="Email" name="email" id="email" class="email required"></div>
	                    <div id="reg-submit" class="one submit"><input id="reg" type="button" value="Отправить запрос" class="btn"></div>
					</form>
				</div>
			<? else: ?>
				<p class="big">Здравствуйте, <?= $user->name ?>. (<a href="/user/logout">Выход</a>)</p>

			<? endif; ?>
        </div>
	</div>
	<?php echo \Yii::$app->view->renderFile('@app/views/layouts/_left_menu_file_sidebar.php'); ?>
</div>