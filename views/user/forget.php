<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
	<div class="col-sm-9 col-sm-push-3 r">
		<div class="text">
			<h1>Клиентам</h1>
			<p class="big">Восстановления доступа к&nbsp;закрытым разделам сайта.</p>
			<div class="clients">
				<div class="row">
					<div class="col-xs-6">
						<div class="login">
							<? if(isset($_GET['token'])): ?>
								<? if(!empty($type_password) && $type_password == 'new_password'): ?>
									<p>Истёк срок действия кода для восстановления пароля. <a href='/user/forget'>Попробуйте восстановить пароль ещё раз</a></p>
								<? else: ?>
									<? if(!empty($message)): ?>
										<?= $message ?>
									<? else: ?>
										<form action="/user/forget?token=<?= $_GET['token'] ?>" method="post">
											<div class="one"><input type="text" autofocus placeholder="Новый пароль" name="password" id=""></div>
											<div class="one submit"><input type="submit" name="forget_password" value="Изменить" class="btn"></div>
										</form>
									<? endif; ?>
								<? endif; ?>
							<? else: ?>

								<? if(!empty($type) && $type == 'forget'): ?>
									<p>На ваш email отправлено письмо с инструкцией.</p>
								<? else: ?>
									<form action="/user/forget" method="post">
										<div class="one"><input type="text" autofocus placeholder="email" name="email" id=""></div>
										<div class="one submit"><input type="submit" name="forget" value="Отправить" class="btn"></div>
									</form>
								<? endif; ?>
							<? endif; ?>


						</div>
					</div>
				</div>
			</div>

        </div>
	</div>
	<?php echo \Yii::$app->view->renderFile('@app/views/layouts/_left_menu_file_sidebar.php'); ?>
</div>