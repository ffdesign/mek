<?php
use yii\helpers\Html;
use app\helpers\YiiHelper;

$this->title = $category[$_GET['id']] . ' | ' . $this->context->module->name;
?>

<div class="row">
	<div class="col-sm-9 col-sm-push-3 r">
		<div class="text">
			<h1><?= $category[$_GET['id']] ?></h1>
			<? if(!empty(YiiHelper::getTextblock("files_category_" . $_GET['id']))): ?>
				<p class="big"><?= YiiHelper::getTextblock("files_category_" . $_GET['id']) ?></p>
			<? endif; ?>
			<div class="docs">
				<div class="row">
					<div class="col-xs-6">
						<? foreach($model as $key => $file): 
						   $doc = 'files/files/'.$file->id.'/'.$file->file;
						?>
							<? if($key % 2 == 0): ?>
								<div class="one">
									<div class="title"><a href="/<?=$doc?>" rel="blank"><?= $file->title ?></a></div>
									<div class="meta"><?= pathinfo($doc, PATHINFO_EXTENSION) ?>, <?= str_replace(".", "," , strval(round(filesize($doc)/1024))) ?> кб</div>
								</div>
							<? endif; ?>
						<? endforeach; ?>
					</div>
					<div class="col-xs-6">
						<? foreach($model as $key => $file): 
						   $doc = 'files/files/'.$file->id.'/'.$file->file;
						?>
							<? if($key % 2 == 1): ?>
								<div class="one">
									<div class="title"><a href="/<?=$doc?>" rel="blank"><?= $file->title ?></a></div>
									<div class="meta"><?= pathinfo($doc, PATHINFO_EXTENSION) ?>, <?= str_replace(".", "," , strval(round(filesize($doc)/1024))) ?> кб</div>
								</div>
							<? endif; ?>
						<? endforeach; ?>
					</div>
				</div>
			</div>
        </div>
	</div>
	<?php echo \Yii::$app->view->renderFile('@app/views/layouts/_left_menu_file_sidebar.php'); ?>
</div>