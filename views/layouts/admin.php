<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\helpers\YiiHelper;
use yii\bootstrap\Alert;
use app\assets\AppAsset;
use app\models\Quests;

/**
 * @var \yii\web\View $this
 * @var string $content
 */
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Панель администратора</title>

    <?php $this->head() ?>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <link href="/css/admin.css" rel="stylesheet"
    
    

  </head>

  <body>
<?php $this->beginBody() ?>
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/">F&F</a>
        </div>
        <div class="navbar-collapse collapse">
          <div class="navbar-right">
            <a class="navbar-brand" href="/site/logout">Выход</a>
          </div>
          <form class="navbar-form navbar-right">
            <!-- <input type="text" class="form-control" placeholder="Search..."> -->
          </form>
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <?php echo Nav::widget([
                'options' => ['class'=>'nav nav-sidebar'],
                'items' => [
                    ['label' => 'Текстовые блоки', 'url' => ['/admin/textblocks/manage'], 'options' => ['class' => YiiHelper::active_menu('textblocks')]],
                    ['label' => 'Текстовые страницы', 'url' => ['/admin/page/manage'], 'options' => ['class' => YiiHelper::active_menu('page')]],
                    ['label' => 'Лицензии и свидетельства', 'url' => ['/admin/license/manage'], 'options' => ['class' => YiiHelper::active_menu('license')]],
                    ['label' => 'Раскрытие информации', 'url' => ['/admin/information/manage'], 'options' => ['class' => YiiHelper::active_menu('information')]],
                    ['label' => 'Рейтинги', 'url' => ['/admin/ratings/manage'], 'options' => ['class' => YiiHelper::active_menu('ratings')]],
                    ['label' => 'Новости', 'url' => ['/admin/news/manage'], 'options' => ['class' => YiiHelper::active_menu('news')]],
                    ['label' => 'Инфраструктура рынка', 'url' => ['/admin/infrastructure/manage'], 'options' => ['class' => YiiHelper::active_menu('infrastructure')]],
                    ['label' => 'Пользовательские файлы', 'url' => ['/admin/files/manage'], 'options' => ['class' => YiiHelper::active_menu('files')]],
                    ['label' => 'Пользователи', 'url' => ['/admin/users/manage'], 'options' => ['class' => YiiHelper::active_menu('users')]],
                ],
            ]);
            ?>
        <? if(YiiHelper::active_menu('quests')): ?>
        <? 
            $QuestsModel = new Quests;
            $QuestsModel = $QuestsModel->find()->select("id, title")->where(['is_visible' => 1])->orderBy('position DESC')->all(); 
        ?>
        <h3>Бронирование</h3>
        <ul id="w1" class="nav nav-sidebar">
            <? foreach ($QuestsModel as $quest): ?>
                <li class="<?= YiiHelper::active_menu('manage/'.$quest->id) ?>"><a href="/admin/quests/order/manage/<?= $quest->id ?>"><?= $quest->title ?></a></li>
            <? endforeach; ?>
        </ul>
        <? endif; ?>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">Панель администратора</h1>
            <?= Breadcrumbs::widget([
                'homeLink' => false,
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>

            <? if(Yii::$app->session->hasFlash('flashMessage')):
                    echo Alert::widget([
                    'options' => [
                        'class' => 'alert-'.Yii::$app->session->getFlash('flashMessage')[0],
                        ],
                        'body' => Yii::$app->session->getFlash('flashMessage')[1],
                    ]);
                endif; 
            ?>

            <?= $content ?>
        
        </div>
      </div>
    </div>

    <?php $this->endBody() ?>
    
    <!-- <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script> -->
<!--     <script type="text/javascript">
        $('li a').click(function (e) {
            e.preventDefault()
            $(this).tab('show');
        })
    </script> -->
  </body>
</html>
<?php $this->endPage() ?>