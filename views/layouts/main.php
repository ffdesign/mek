<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\helpers\YiiHelper;
use app\widgets\MenuWidget;

AppAsset::register($this);

switch (Yii::$app->request->url) {
    case '/site/service1':
        $service_bg = 'style="background-image:url(/i/service-1-bg.jpg);"';
        $service_top = 1;
        break;
    case '/site/service2':
        $service_bg = 'style="background-image:url(/i/service-2-bg.jpg);"';
        $service_top = 2;
        break;
    case '/site/service3':
        $service_bg = 'style="background-image:url(/i/service-3-bg.jpg);"';
        $service_top = 3;
        break;
    default:
        $service_bg = '';
        $service_top = false;
        break;
}
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <title><?= Html::encode($this->title) ?></title>

    <?= Html::csrfMetaTags() ?>
    <?php $this->head() ?>
    <link rel="stylesheet" href="/css/bootstrap.css" media="all">
    <link rel="stylesheet" href="/css/magnific-popup.css" media="all">
    <link rel="stylesheet" href="/css/style.css" media="all">


    <script src="/js/jquery-1.11.3.min.js"></script>
    <script src="/js/jquery.magnific-popup.min.js"></script>
    <script src="/js/jquery.matchHeight-min.js"></script>
    <script src="/js/jquery.validate.js"></script>
    <script src="/js/project.js"></script>

    <meta name="viewport" id="viewport" content="width=device-width, initial-scale=1">
    <meta name="HandheldFriendly" content="true"/>

</head>
<body class="<?php echo Yii::$app->request->url == '/' ? 'home' : '' ?>">
<?php $this->beginBody() ?>
<div id="wrapper">
    <div class="head" <?php echo $service_bg; ?>>
        <div class="top">
            <div class="container clearfix">
                <div class="logo pull-left"><a href="/"><img src="/i/logo.png" alt="Межрегиональная энергосбытовая компания"></a></div>
                <? if(!empty(YiiHelper::getTextblock("header_phone"))): ?>
                    <div class="phone pull-right"><a href="tel:<?= YiiHelper::getTextblock("header_phone") ?>"><i class="icons icon-phone-big"></i> <?= YiiHelper::getTextblock("header_phone") ?></a></div>
                <? endif; ?>
                
                <div class="menu">
                    <ul>
                        <li <?php echo YiiHelper::active_menu('page') ? 'class="active"' : ''; ?>
                            <?php echo YiiHelper::active_menu('page/kompaniya-segodnya') ? 'class="active"' : ''; ?>
                            <?php echo YiiHelper::active_menu('page/deyatelnost') ? 'class="active"' : ''; ?>
                            <?php echo YiiHelper::active_menu('page/rekvizity') ? 'class="active"' : ''; ?>
                            <?php echo YiiHelper::active_menu('license') ? 'class="active"' : ''; ?>
                            <?php echo YiiHelper::active_menu('ratings') ? 'class="active"' : ''; ?>
                            <?php echo YiiHelper::active_menu('information') ? 'class="active"' : ''; ?>
                            <?php echo YiiHelper::active_menu('news') ? 'class="active"' : ''; ?>
                        >
                            <a href="/page/kompaniya-segodnya">Компания</a>
                            <ul>
                                <li <?php echo YiiHelper::active_menu('page/deyatelnost') ? 'class="active"' : ''; ?>><a href="/page/deyatelnost">Деятельность</a></li>
                                <li <?php echo YiiHelper::active_menu('page/rekvizity') ? 'class="active"' : ''; ?>><a href="/page/rekvizity">Реквизиты</a></li>
                                <?php echo MenuWidget::widget(); ?>
                                <li <?php echo YiiHelper::active_menu('license') ? 'class="active"' : ''; ?>><a href="/license">Лицензии и свидетельства</a></li>
                                <li <?php echo YiiHelper::active_menu('ratings') ? 'class="active"' : ''; ?>><a href="/ratings">Рейтинги</a></li>
                                <li <?php echo YiiHelper::active_menu('information') ? 'class="active"' : ''; ?>><a href="/information">Раскрытие информации</a></li>
                                <li <?php echo YiiHelper::active_menu('news') ? 'class="active"' : ''; ?>><a href="/news">Новости</a></li>
                            </ul>
                        </li>
                        <li <?php echo YiiHelper::active_menu('user') ? 'class="active"' : ''; ?>>
                            <a href="/user">Клиентам</a>
                            <ul>
                                <li><a href="/user/files/1">Типовые документы</a></li>
                                <li><a href="/user/files/2">Отчетная информация</a></li>
                                <li><a href="/user/files/3">Аналитика</a></li>
                                <li><a href="/user/files/4">Интернет-трейдинг</a></li>
                            </ul>
                        </li>
                        <li <?php echo YiiHelper::active_menu('services') ? 'class="active"' : ''; ?><?php echo YiiHelper::active_menu('service') ? 'class="active"' : ''; ?>>
                            <a href="/site/services">Услуги</a>
                            <ul>
                                <li <?php echo YiiHelper::active_menu('service1') ? 'class="active"' : ''; ?>><a href="/site/service1">Поставка электроэнергии</a></li>
                                <li <?php echo YiiHelper::active_menu('service2') ? 'class="active"' : ''; ?>><a href="/site/service2">Консалтинг и ИТ-услуги</a></li>
                                <li <?php echo YiiHelper::active_menu('service3') ? 'class="active"' : ''; ?>><a href="/site/service3">Учет электроэнергии</a></li>
                            </ul>
                        </li>
                        <li <?php echo YiiHelper::active_menu('contact') ? 'class="active"' : ''; ?>><a href="/site/contact">Контакты</a></li>
                    </ul>
                </div>
                <div class="burger"><a href="#"></a></div>
            </div>
        </div>
        <? switch ($service_top) {
            case 1:
                echo $this->render('services/_top_service1');
                break;
            case 2:
                echo $this->render('services/_top_service2');
                break;
            case 3:
                echo $this->render('services/_top_service3');
                break;
            
            default:
                break;
        }
        ?>

        <? if(Yii::$app->request->url == '/'): ?>
            <div class="intro">
                <div class="container">
                    <div class="title"><?= YiiHelper::getTextblock("header_slogan") ?></div>
                </div>
            </div>
        <? endif; ?>
        <div class="bot hidden-xs">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <a href="/site/service1">
                            <div>
                                <? if(Yii::$app->request->url == '/'): ?>
                                    <i class="icons icon-service-1"></i>Поставка<br> электроэнергии
                                    <span>Поставка электрической энергии и&nbsp;мощности с&nbsp;оптового и&nbsp;розничного рынков.</span>
                                <? else: ?>
                                    <i class="icons icon-service-1"></i>Поставка электроэнергии
                                <? endif; ?>
                                
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-4">
                        <a href="/site/service2">
                            <div>
                                <? if(Yii::$app->request->url == '/'): ?>
                                    <i class="icons icon-service-2"></i>Консалтинг<br> и ИТ услуги
                                    <span>Консультации на&nbsp;стадии выхода на&nbsp;оптовый рынок электроэнергии и&nbsp;сопровождение торговых операций на&nbsp;рынке</span>
                                <? else: ?>
                                    <i class="icons icon-service-2"></i>Консалтинг и ИТ услуги
                                <? endif; ?>
                                
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-4">
                        <a href="/site/service3">
                            <div>
                                <? if(Yii::$app->request->url == '/'): ?>
                                    <i class="icons icon-service-3"></i>Учет<br> электроэнергии
                                    <span>Создание и&nbsp;модернизация информационно-измерительных систем коммерческого учета электроэнергии</span>
                                <? else: ?>
                                    <i class="icons icon-service-3"></i>Учет электроэнергии
                                <? endif; ?>
                                
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <? if(Yii::$app->request->url == '/site/contact'): ?>
        <?= $content; ?>
    <? else: ?>
        <div class="content">
            <div class="container">
                <?= $content; ?>
            </div>
        </div>
    <? endif; ?>
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-push-4">
                    <div class="fmenu clearfix">
                        <ul>
                            <li>
                                <a href="/page/kompaniya-segodnya">Компания</a>
                                <ul>
                                    <li><a href="/page/deyatelnost">Деятельность</a></li>
                                    <li><a href="/page/rekvizity">Реквизиты</a></li>
                                    <li><a href="/license">Лицензии и свидетельства</a></li>
                                    <li><a href="/ratings">Рейтинги</a></li>
                                    <li><a href="/information">Раскрытие информации</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="/user">Клиентам</a>
                                <ul>
                                    <li><a href="/user/files/1">Типовые документы</a></li>
                                    <li><a href="/user/files/2">Отчетная информация</a></li>
                                    <li><a href="/user/files/3">Аналитика</a></li>
                                    <li><a href="/user/files/4">Интернет-трейдинг</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="/site/services">Услуги</a>
                                <ul>
                                    <li><a href="/site/service1">Поставка электроэнергии</a></li>
                                    <li><a href="/site/service2">Консалтинг и ИТ-услуги</a></li>
                                    <li><a href="/site/service3">Учет электроэнергии</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="/site/contact">Контакты</a>
                            </li>
                            <li class="helper"></li>
                        </ul>
                    </div>
                </div>

                <div class="col-md-4 col-md-pull-8">
                    <div class="fcopy">&copy; Межрегиональная Энергосбытовая Компания, <?= date('Y') ?></div>
                    <? if(!empty(YiiHelper::getTextblock("footer_contacts"))): ?>
                        <div class="fcont"><?= YiiHelper::getTextblock("footer_contacts") ?></div>
                    <? endif; ?>
                    <div class="fdev">Разработка сайта &mdash; <a href="http://f-f.bz/" rel="blank">Foyht&Frank</a></div>
                </div>
            </div>
        </div>
    </div>
</div>
<?//php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>