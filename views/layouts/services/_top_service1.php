<?php
use yii\helpers\Html;
use app\helpers\YiiHelper;
?>
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-push-3 r">
                <div class="text">
                    <h1>Поставка электроэнергии</h1>
                    <p class="big"><?= YiiHelper::getTextblock("services_service1_desciption") ?></p>

                </div>
            </div>
            <script src="/js/jquery.animateNumber.min.js"></script>
            <div class="col-sm-3 col-sm-push-3 r">
                <div class="row nums">
                    <div class="col-md-6 col-sm-12 col-xs-6">
                        <div class="num">
                            <div class="num-top"><span><em class="count" data-count="<?= date('Y')-2010 ?>">0</em></span>лет</div>
                            <div class="num-bot">опыт работы в&nbsp;отрасли</div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-6">
                        <div class="num">
                            <div class="num-top"><span><em class="count" data-count="10">0</em></span>%</div>
                            <div class="num-bot">средняя экономия</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-3 col-sm-pull-9 l hidden-xs">
                <div class="back"><a href="/site/services">&larr;<span>Назад</span></a></div>
                <div class="sidebar">
                    <ul>
                        <li class="active"><a href="/site/service1">Поставка электроэнергии</a></li>
                        <li><a href="/site/service2">Консалтинг и&nbsp;ИТ услуги</a></li>
                        <li><a href="/site/service3">Учет электроэнергии</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>