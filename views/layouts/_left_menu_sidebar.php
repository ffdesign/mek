<?php
use yii\helpers\Html;
use app\helpers\YiiHelper;
use app\widgets\MenuWidget;
?>

<div class="col-sm-3 col-sm-pull-9 l">
	<div class="back"><a href="<?= (YiiHelper::active_menu('news') == 'active') ? '/news' : '/' ?>">←<span>Назад</span></a></div>
	<div class="sidebar">
		<ul>
			<li <?php echo YiiHelper::active_menu('page/deyatelnost') ? 'class="active"' : ''; ?>><a href="/page/deyatelnost">Деятельность</a></li>
			<li <?php echo YiiHelper::active_menu('page/rekvizity') ? 'class="active"' : ''; ?>><a href="/page/rekvizity">Реквизиты</a></li>
			<?php echo MenuWidget::widget(); ?>
			<li <?php echo YiiHelper::active_menu('license') ? 'class="active"' : ''; ?>><a href="/license">Лицензии и свидетельства</a></li>
			<li <?php echo YiiHelper::active_menu('ratings') ? 'class="active"' : ''; ?>><a href="/ratings">Рейтинги</a></li>
			<li <?php echo YiiHelper::active_menu('information') ? 'class="active"' : ''; ?>><a href="/information">Раскрытие информации</a></li>
			<li <?php echo YiiHelper::active_menu('news') ? 'class="active"' : ''; ?>><a href="/news">Новости</a></li>
		</ul>
	</div>
</div>