<?php
use yii\helpers\Html;
use app\helpers\YiiHelper;
?>


<div class="col-sm-3 col-sm-pull-9 l">
	<div class="back"><a href="/user">&larr;<span>Назад</span></a></div>
	<div class="sidebar">
		<ul>
			<li <?php echo YiiHelper::active_menu('user/files/1') ? 'class="active"' : ''; ?>><a href="/user/files/1">Типовые документы</a></li>
			<li <?php echo YiiHelper::active_menu('user/files/2') ? 'class="active"' : ''; ?>><a href="/user/files/2">Отчетная информация</a></li>
			<li <?php echo YiiHelper::active_menu('user/files/3') ? 'class="active"' : ''; ?>><a href="/user/files/3">Аналитика</a></li>
			<li <?php echo YiiHelper::active_menu('user/files/4') ? 'class="active"' : ''; ?>><a href="/user/files/4">Интернет-трейдинг</a></li>
		</ul>
	</div>
</div>