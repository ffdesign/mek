$(document).ready(function(){

    $('a[href$=".pdf"]').attr('target', '_blank');

    $("a[rel='blank']").click(function(){
		window.open($(this).attr('href'));
		return false
	});

    $('.burger a').click(function(){
		if ($(this).hasClass('active')) {
            $(this).removeClass('active');
			$('.menu').slideUp(200);
		} else {
            $(this).addClass('active');
			$('.menu').slideDown(200);
		}
		return false
	});

    $('.license').magnificPopup({
		delegate: 'a',
		type: 'image',
		closeOnContentClick: true,
        image: {
			verticalFit: false
		},
        closeBtnInside: false,
        overflowY: 'scroll'
	});

	$('.clients-list.visible-xs').html($('.clients-list.hidden-xs').html());

	$('body.home .head .bot a, .service-2 .row > div').matchHeight();

    $('.text table').each(function(){
		$(this).wrap( "<div class='table-responsive'></div>" );
	});

	$('a[data-popup]').click(function(){
		popup = $(this).data('popup');
        $.magnificPopup.open({
			items: {
				src: '#'+popup,
				type: 'inline'
			},
            focus: $('#'+popup).find('input')
		});
		return false
	});

	$('.toggle-btn a').click(function(){
		toggleBlock = $(this).closest('.toggle');
		if ($(this).hasClass('active')) {
			$(this).removeClass('active');
			$('.toggle-hidden',toggleBlock).hide();
		} else {
			$(this).addClass('active');
			$('.toggle-hidden',toggleBlock).show();
		}
		return false
	});

/*
	$('.count').each(function () {
	    $(this).prop('Counter',0).animate({
	        Counter: $(this).text()
	    }, {
	        duration: 1100,
	        easing: 'swing',
	        step: function (now) {
	            $(this).text(Math.ceil(now));
	        }
	    });
	});
*/

    $('.count').each(function () {
	    $(this).animateNumber({ number: $(this).data('count') }, 1000);
	});

	if ($('.rate-tip').length) {
        
		rateBtn = $('.rate-btn');
		rateTip = $('.rate-tip');
        rateTip.appendTo('#wrapper');
		$('<div class="overlay"></div>').appendTo('#wrapper');
		rateTipHalf = rateTip.outerWidth() / 2;

		selectMonth = $('#rate_month');
		selectYear = $('#rate_year');

        rateBtn.click(function(){
            rateBtnT = rateBtn.text();
			rateBtnT = rateBtnT.split(' ');
			selectMonth.val(selectMonth.find('option').filter(function() { return $(this).text().toLowerCase() == rateBtnT[0]; }).val());
			selectYear.val(selectYear.find('option').filter(function() { return $(this).text().toLowerCase() == rateBtnT[1]; }).val());
			rateBtnO = rateBtn.offset();
	        rateBtnW = rateBtn.width();
	        rateBtnH = rateBtn.height();
            $('.rate-tip').show().css({top: rateBtnO.top + rateBtnH + 13, left: rateBtnO.left + rateBtnW/2 - rateTipHalf});
			$('.overlay').show();
			return false
		});
		
		$(window).resize(function(){
			if (rateTip.is(':visible')) {
				rateBtnO = rateBtn.offset();
		        rateBtnW = rateBtn.width();
		        rateBtnH = rateBtn.height();
	            $('.rate-tip').css({top: rateBtnO.top + rateBtnH + 13, left: rateBtnO.left + rateBtnW/2 - rateTipHalf});
			}
		});

		$('.overlay').click(function(){
            $('.overlay, .rate-tip').hide();
		});

		rateTip.find('.btn').click(function(){
			rateBtn.text(selectMonth.find('option:selected').text().toLowerCase() + ' ' + selectYear.find('option:selected').text().toLowerCase());
            $('.rate-content').addClass('loading');
			/*
				url = '';
                $.ajax({
					type: "POST",
					url: url,
					data: rateTip.find('form').serialize(),
		            success: function(msg){
						$('.rate-content').html(msg).removeClass('loading');
					}
				});
			*/
            $('.rate-content').removeClass('loading'); // REMOVE AFTER AJAX IS ON!!
            $('.overlay').click();
			return false
		});
		
	}

});