<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;
use Yii;
use app\models\NewsLinks;
use app\models\News;
use yii\web\Controller;
use app\helpers\YiiHelper;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class NewsController extends Controller
{   
    public $enableCsrfValidation = false;
    
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     */
    public function actionIndex()
    {
        include('shd.php');
        foreach (NewsLinks::find()->where(['status' => 1])->all() as $item) {
            $html = file_get_html('http://www.mrg-sbyt.ru/' . $item->url);
            //title
            foreach($html->find('.content > .title > h2') as $e)
                $title = $e->plaintext;

            //date
            foreach($html->find('.content > .date') as $e)
                $date = $e->plaintext;

            foreach($html->find('.content > .source > a') as $e) {
                $source_href = $e->href;
                $source_a = $e->plaintext;
            }

            //content
            foreach($html->find('.content > .text') as $e)
                $content = $e->plaintext;

            $content = str_replace($date, '', $content);
            $content = str_replace($source_a, '', $content);

            $model = new News;
            $model->title = $title;
            $model->text = nl2br($content);
            $model->created = $date;
            $model->save();

            $newsLinks = NewsLinks::find()->where(['url' => $item->url])->one();
            $newsLinks->status = 0;
            $newsLinks->save();


        }
    }
}