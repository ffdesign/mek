<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;
use Yii;
use app\models\NewsLinks;
use yii\web\Controller;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class NewslinksController extends Controller
{   
    public $enableCsrfValidation = false;
    
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     */
    public function actionIndex()
    {
        include('shd.php');
        // for ($i=1; $i <= 128 ; $i++) { 
            $i = 1;
            $html = file_get_html('http://www.mrg-sbyt.ru/news/?PAGEN_2=' . $i);
            foreach($html->find('.content > h3 > a') as $e) {
                if(!isset(NewsLinks::find()->where(['url' => $e->href])->one()->id)) {
                    $model = new NewsLinks;
                    $model->url = $e->href;
                    $model->save();
                }
            }
        // }
    }
}