<?php
namespace app\helpers;
use Yii;
use app\models\Textblocks;
use yii\imagine\Image;

class YiiHelper
{

	/**
	 * Вызов и установка текстового блока
	 * @param   string     $name имя текстового блока, принимает русские и латинские названия
	 * @return  mixed            содержимое текствого блока
	 * @created 2015-02-24
	 * @author Roman Frank (Akella) 2akellaw@gmail.com
	 */
	static function getTextblock($name, $editor = null)
    {
        $model = new Textblocks;
        $query = $model->find()->where(['name' => $name])->one();
        if($query) {
        	if($query->is_visible) {
        		if($query->image)
            		return "<img src='/images/textblocks/".$query->id.'/'.$query->image."'/>";
            	else 
            		return $query->text;
        	}
        } else {
            $model->name = $name;
            $model->location = Yii::$app->request->url;
            $model->title = $name;
            $model->is_visible = 0;
            if($editor)
            	$model->editor = 1;
            $model->save();
        }
    }

    /**
     * Загрузка изображений на сервере 
     * @param   model     $model     модель таблицы
     * @param   array     $files     $_FILES['_example']
     * @param   string     $field     _example
     * @param   int     $lastId    последний id
     * @param   string     $uploadDir путь до директории
     * @param   array     $previews  массив для создания превью, формат: array('100, 200, 300', '400, 200, 300', '600, 200, 300'); 
     * @param   array     $previews  первое значение - высота, второе - ширина, третье - качество сжатия
     * @return  upload
     * @created 2015-05-20
     * @author Roman Frank (Akella) 2akellaw@gmail.com
     */
    static function uploadImage($model, $files, $field, $lastId, $uploadDir, $previews = null)
    {
    	// print_r($files); exit;
    	$upload_dir_id = $uploadDir . $lastId;
        if(!file_exists($upload_dir_id))
            mkdir($upload_dir_id);
        else {
        	if(file_exists($upload_dir_id))
        		self::removeImage($upload_dir_id);
        	mkdir($upload_dir_id);
        }

        $ext = pathinfo($files['name'][$field], PATHINFO_EXTENSION);
        $imageName = md5($files['name'][$field] . date('dmYhis')) . '.' . $ext;

        $model_field = str_replace('_', '', $field);

        $model->$model_field = $imageName;

        move_uploaded_file($files['tmp_name'][$field], $upload_dir_id . '/' . $imageName);
        if(is_array($previews)) {
        	Image::thumbnail($upload_dir_id . '/' . $imageName, 200, 200)->save($upload_dir_id . '/' . 'thumb-' . $imageName, ['quality' => 100]);
			foreach ($previews as $preview) {
				$preview_params = explode(',', $preview); //$preview_params[0] - высота, $preview_params[1] - ширина, $preview_params[2] - качество
				Image::thumbnail($upload_dir_id . '/' . $imageName, $preview_params[0], $preview_params[1])->save($upload_dir_id . '/' . trim($preview_params[0]) . 'x' . trim($preview_params[1]) . '-' . $imageName, ['quality' => $preview_params[2]]);
			}
		} else {
			Image::thumbnail($upload_dir_id . '/' . $imageName, 200, 200)->save($upload_dir_id . '/' . 'thumb-' . $imageName, ['quality' => 100]);
		}
    }

    static function removeImage($dir) {
        if ($objs = glob($dir."/*")) {
            foreach($objs as $obj) {
                is_dir($obj) ? removeDirectory($obj) : unlink($obj);
            }
        }
        rmdir($dir);
    }

	/**
	 * Статус активности для меню
	 * @param   string     $nameModule имя модуля
	 * @return  string 
	 * @created 2015-02-23
	 * @author Roman Frank (Akella) 2akellaw@gmail.com
	 */
	static function active_menu($nameModule) {
		if($nameModule != 'admin') {
			$preg_match = preg_match("/".preg_quote($nameModule, '/')."/", Yii::$app->request->url);
			if($preg_match) {
				return 'active';
			}
		}
	}

	static function utf8_str_split($str) { 
		$split=1; 
		$array = array(); 
		for ($i=0; $i < strlen($str);) { 
			$value = ord($str[$i]); 
			if($value > 127) { 
				if($value >= 192 && $value <= 223) 
					$split=2; 
				elseif($value >= 224 && $value <= 239) 
					$split=3; 
				elseif($value >= 240 && $value <= 247) 
					$split=4; 
			} else { 
				$split=1; 
			} 
			$key = NULL; 
			for ($j = 0; $j < $split; $j++, $i++) { 
				$key .= $str[$i]; 
			} 
			array_push( $array, $key ); 
		} 
		return $array; 
	} 

	static function clearstr($str){ 
		$sru = 'ёйцукенгшщзхъфывапролджэячсмитьбю'; 
		$s1 = array_merge(self::utf8_str_split($sru), self::utf8_str_split(strtoupper($sru)), range('A', 'Z'), range('a','z'), range('0', '9'), array(' ')); 
		$codes = array(); 
		for ($i=0; $i<count($s1); $i++){ 
			$codes[] = ord($s1[$i]); 
		} 
		$str_s = self::utf8_str_split($str); 
		for ($i=0; $i<count($str_s); $i++){ 
			if (!in_array(ord($str_s[$i]), $codes)){ 
			    $str = str_replace($str_s[$i], '', $str); 
			} 
		} 
		return $str; 
	} 

	static function translite($str) {
		$str = self::clearstr($str);
		$str = trim($str);
		$str = strtolower($str);
		// $str = self::character_limiter($str, $character_limiter, "");

		$translit = array("а" => "a", "б" => "b", "в" => "v", "г" => "g", "д" => "d", "е" => "e", "ё" => "yo", "Ё" => "yo", "ж" => "zh", "з" => "z",
			"и" => "i", "й" => "y", "к" => "k", "л" => "l", "м" => "m", "н" => "n", "о" => "o", "п" => "p",
			"р" => "r", "с" => "s", "т" => "t", "у" => "u", "ф" => "f", "х" => "h", "ц" => "c", "ч" => "ch",
			"ш" => "sh", "щ" => "sch", "ъ" => "", "ы" => "y", "ь" => "", "э" => "e", "ю" => "yu", "я" => "ya",
			"А" => "a", "Б" => "b", "В" => "v", "Г" => "g", "Д" => "d", "Е" => "e", "Ж" => "zh", "З" => "z",
			"И" => "i", "Й" => "y", "К" => "k", "Л" => "l", "М" => "m", "Н" => "n", "О" => "o", "П" => "p",
			"Р" => "r", "С" => "s", "Т" => "t", "У" => "u", "Ф" => "f", "Х" => "h", "Ц" => "c", "Ч" => "ch",
			"Ш" => "sh", "Щ" => "sch", "Ъ" => "", "Ы" => "y", "Ь" => "", "Э" => "e", "Ю" => "yu", "Я" => "ya",
			" " => "-", "," => "");
		$str = strtr($str, $translit);
		return $str;
  }

}