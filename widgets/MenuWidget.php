<?php
namespace app\widgets;

use Yii;
use yii\web\Response;
use yii\web\Session;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\Pages;

class MenuWidget extends Widget
{
    public function run()
    {
        $model = new Pages;
        $model = $model->find()->where(['is_visible' => 1, 'is_menu' => 1])->all();
        return $this->render('menu', [
            'model' => $model
        ]);
    }

    
}