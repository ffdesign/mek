<?
use app\helpers\YiiHelper;
?>
<? foreach ($model as $item): ?>
	<li <?php echo YiiHelper::active_menu($item->path) ? 'class="active"' : ''; ?>><a href="/page/<?= $item->path ?>"><?= $item->title ?></a></li>
<? endforeach; ?>